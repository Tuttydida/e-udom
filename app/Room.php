<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function genderwings()
    {
    	return $this->belongsToMany(Genderwing::class);
    }
    public function wings(){
    	return $this->belongsToMany(Wing::class);
    }
}

