<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TuitionFee extends Model
{
    protected $fillable = ['program_id', 'tuition_fee_type_id'];
}
