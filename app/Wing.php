<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wing extends Model
{
    public function rooms()
    {
    	return $this->belongsToMany(Room::class);
    }
    public function blocks()
    {
      return $this->belongsToMany(Block::class)->withPivot('id');
    }
    public function gender()
    {
    	return $this->belongsToMany(Gender::class)->withPivot('block_id','id')->withTimestamps();
    }
    

}
