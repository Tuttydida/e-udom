<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
   public function students(){

   	return $this->belongsToMany(Student::class);
   	
   }
  public function block()
  {
  	return $this->belongsTo(Block::class);
  }

  public function wing(){

  	return $this->belongsTo(Wing::class);
  }
  public function room(){

        return $this->belongsTo(Room::class);
  }
}
