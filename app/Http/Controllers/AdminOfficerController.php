<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;

class AdminOfficerController extends Controller
{
    public function index()
    {
    	$documents = Document::withCount('replies')->where('principal_state', 'approved')->latest()->get();
    	return view('admin-officer.documents.index', compact('documents'));
    }

    public function show($id)
    {
    	$document = Document::with('replies')->findOrFail($id);
    	return view('admin-officer.documents.show', compact('document'));
    }
}
