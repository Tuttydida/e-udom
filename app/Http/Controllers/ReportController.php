<?php

namespace App\Http\Controllers;

use App\Registration;
use Illuminate\Http\Request;
use App\UniversityRegistration;

class ReportController extends Controller
{
    public function generateRegReport(Request $request)
    {
        $universityRegistration = UniversityRegistration::findOrFail($request->input('id')); 
        $registrations = Registration::latest()->get();  

        \Excel::create('Report', function ($excel) use ($registrations) {

            $excel->sheet('Registrations', function ($sheet) use ($registrations) {
                $sheet->fromModel($registrations);
            });

        })->download('xlsx');           	
    }
}
