<?php

namespace App\Http\Controllers;

use App\TuitionFeeType;
use Illuminate\Http\Request;

class TuitionFeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TuitionFeeType  $tuitionFeeType
     * @return \Illuminate\Http\Response
     */
    public function show(TuitionFeeType $tuitionFeeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TuitionFeeType  $tuitionFeeType
     * @return \Illuminate\Http\Response
     */
    public function edit(TuitionFeeType $tuitionFeeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TuitionFeeType  $tuitionFeeType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TuitionFeeType $tuitionFeeType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TuitionFeeType  $tuitionFeeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(TuitionFeeType $tuitionFeeType)
    {
        //
    }
}
