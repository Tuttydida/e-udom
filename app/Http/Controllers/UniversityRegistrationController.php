<?php

namespace App\Http\Controllers;

use App\UniversityRegistration;
use Illuminate\Http\Request;

class UniversityRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $university_registrations = UniversityRegistration::with('colleges')->latest()->get();
        return view('university-registrations.index', compact('university_registrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param UniversityRegistration $university_registration
     * @return \Illuminate\Http\Response
     * @internal param UniversityRegistration $universityRegistration
     */
    public function show(UniversityRegistration $university_registration)
    {
        return $university_registration;
        return view('university-registrations.show', compact('university_registration'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UniversityRegistration $universityRegistration
     * @return \Illuminate\Http\Response
     */
    public function edit(UniversityRegistration $universityRegistration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\UniversityRegistration $universityRegistration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UniversityRegistration $universityRegistration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UniversityRegistration $universityRegistration
     * @return \Illuminate\Http\Response
     */
    public function destroy(UniversityRegistration $universityRegistration)
    {
        //
    }

    public function myPayments()
    {
        return view('university-registrations.my-payments');
    }
}
