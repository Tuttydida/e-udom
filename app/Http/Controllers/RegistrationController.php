<?php

namespace App\Http\Controllers;

use App\Payment;
use App\PaymentCategory;
use App\PaymentType;
use App\Registration;
use App\Student;
use App\StudentDirectCostPayment;
use App\StudentTuitionFeePayment;
use App\UniversityRegistration;
use Auth;
use App\Accomodation;
use App\Block;
use App\Gender;
use App\Wing;
use App\Genderwing;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registrations = auth()->user()->registrations->where('status',true);
        $openUniversityRegistrations = UniversityRegistration::get();
        $total_regs = UniversityRegistration::whereStatus(true)->count();
        return view('registrations.index', compact('registrations', 'openUniversityRegistrations', 'total_regs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Registration $registration
     */
    public function show($id)
    {
   $user_id = Auth::user()->id;
 $registration = UniversityRegistration::with('studentRegistration')->findOrFail($id);
 if($registration->studentRegistration==null){
 $registration = UniversityRegistration::findOrFail($id);
  $registered='not registered';
 }else{
 if($registration->studentRegistration->where('user_id',$user_id)->first()==null){
   $registered='not registered';
   $registration = UniversityRegistration::findOrFail($id);
   }else{
    $registered='registered';
   }
 }

        // check for tuition fee payment
        $studentTuitionFeePayment = StudentTuitionFeePayment::where('university_registration_id', $id)
            ->where('user_id', auth()->id())
            ->where('student_id', auth()->user()->student->id)
            ->where('completed', true)
            ->first();

        // check for direct cost payment
        $studentDirectCostPayment = StudentDirectCostPayment::where('university_registration_id', $id)
            ->where('user_id', auth()->id())
            ->where('student_id', auth()->user()->student->id)
            ->where('completed', true)
            ->first();

        $problems = [];
        
        if ($studentTuitionFeePayment == null) {
            $problems[] = 'You have not paid the tuition fee';
        }
        if ($studentDirectCostPayment == null) {
            $problems[] = 'You have not paid the direct cost';
        }
        $problems = collect($problems);
        return view('registrations.show', compact('registration','registered', 'problems'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Registration $registration
     * @return \Illuminate\Http\Response
     */
    public function edit(Registration $registration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Registration $registration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registration $registration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Registration $registration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Registration $registration)
    {
        //
    }

    public function outstandings($id)
    {
        $university_registration = UniversityRegistration::find($id);
        // given that there is a student
        $student = Auth::user()->student;
        // who belongs to a certain program
        $program = $student->program;
        // that program has x tuition fees per year
        $tuition_fee = $program->tuitionFee->amount;
        // who lives in campus
        // who is in a certain year of study


        // find the tuition fee amount that he/she has paid once
        $tuition_fee_paid = Payment::whereUserId($student->user_id)
            ->whereCategory('TF')
            ->whereYearOfStudy('1')//from student
            ->whereSemester($university_registration->semester)// from UR
            ->first();

        $tuition_fee_paid = $tuition_fee_paid->amount;

        // determine the outstanding
        $outstanding = ($tuition_fee / 2) - $tuition_fee_paid;
        return view('registrations.outstandings', compact('university_registration', 'tuition_fee_paid', 'outstanding'));
    }


    public function studentDetails($id)
    {
        $registration = UniversityRegistration::findOrFail($id);
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();
        return view('registrations.partials.student-details', compact('registration', 'student'));
    }

    public function updateStudentDetails(Request $request)
    {
        $this->validate($request, [
            'dob' => 'required',
        ]);

        $student = Student::findOrFail($request->input('student_id'));
        $student->last_name = $request->input('last_name');
        $student->first_name = $request->input('first_name');
        $student->middle_name = $request->input('middle_name');
        $student->gender = $request->input('gender');
        $student->nationality = $request->input('nationality');
        $student->dob = $request->input('dob');
        $student->yos = $request->input('yos');
        $student->f4_school = $request->input('f4_school');
        $student->f4_index = $request->input('f4_index');
        $student->f4_year = $request->input('f4_year');
        $student->f6_school = $request->input('f6_school');
        $student->f6_index = $request->input('f6_index');
        $student->f6_year = $request->input('f6_year');
        $student->save();

        flash('Details successfully updated');

        return back();
    }

    public function contactAddress($id)
    {
        $registration = UniversityRegistration::findOrFail($id);
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();
        return view('registrations.partials.contact-address', compact('registration', 'student'));
    }

    public function updateContactAddress(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|unique:students,id',
        ]);

        $student = Student::findOrFail($request->input('student_id'));
        $student->mobile = $request->input('mobile');
        $student->email = $request->input('email');
        $student->bank_account_number = $request->input('bank_account_number');
        $student->bank_name = $request->input('bank_name');
        $student->save();

        flash('Contact address successfully updated');

        return back();
    }

    public function parentOrGuardian($id)
    {

        $registration = UniversityRegistration::findOrFail($id);
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();
        return view('registrations.partials.parent-or-guardian', compact('registration', 'student'));
    }

    public function updateParentOrGuardian(Request $request)
    {


        $this->validate($request, [
            'parent_name' => 'required',
            'parent_relationship' => 'required',
            'parent_address' => 'required',
            'parent_mobile' => 'required',
        ]);

        $student = Student::findOrFail($request->input('student_id'));
        $student->parent_name = $request->input('parent_name');
        $student->parent_relationship = $request->input('parent_relationship');
        $student->parent_address = $request->input('parent_address');
        $student->parent_mobile = $request->input('parent_mobile');
        $student->save();

        flash('Parent or Guardian info successfully updated');

        return back();
    }

    public function photo($id)
    {

        $registration = UniversityRegistration::findOrFail($id);
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();
        return view('registrations.partials.photo', compact('registration', 'student'));
    }

    public function updatePhoto(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image'
        ]);

        if ($request->hasFile('photo')) {
            if ($request->file('photo')->isValid()) {
                $student = Student::findOrFail($request->input('student_id'));
                $student->photo = $request->photo->store('photos');
                $student->save();
            }
        }


        flash('Photo successfully updated');

        return back();
    }

    public function completeRegistration($id)
    {
        $registration = UniversityRegistration::with('studentRegistration')->first();
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();
        return view('registrations.partials.complete-registration', compact('registration', 'student'));
    }

    public function confirmRegistration($id)
    {
        $universityRegistration = UniversityRegistration::first();

        $registration = new Registration();
        $registration->university_registration_id = $universityRegistration->id;
        $registration->academic_year = $universityRegistration->academic_year;
        $registration->semester = $universityRegistration->semester;
        $registration->year_of_study = rand(1, 5);
        $registration->registration_status = true;
        $registration->date_of_registration = Carbon::now();
        $registration->user_id = auth()->id();

        $universityRegistration->studentRegistration()->save($registration);

        flash('Registration successfully completed');

        return back();
    }

    public function tuitionFee($id)
    {
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();

        $required_amount = $student->program->tuitionFee->amount;

        $registration = UniversityRegistration::with('studentRegistration')->findOrFail($id);
        $studentTuitionFeePayment = StudentTuitionFeePayment::whereUniversityRegistrationId($registration->id)->firstOrCreate([
            'university_registration_id' => $registration->id,
            'user_id' => auth()->id(),
            'student_id' => auth()->user()->student->id,
            'required_amount' => $required_amount,
        ]);
        // Get tuition fee
        $category = PaymentCategory::whereName('tuition-fee')->first();
        // Get unused tuition fees
        $tuitionFeePayments = Payment::whereUserId(auth()->id())
            ->whereCategory($category->id)
            ->whereExpired(false)
            ->get();

        if ($studentTuitionFeePayment) {
            $studentTuitionFeePayment->required_amount = $tuitionFeePayments->sum('amount');
        }



        return view('registrations.partials.tuition-fee', compact('registration', 'student', 'tuitionFeePayments', 'studentTuitionFeePayment', 'required_amount'));
    }

    public function payForTuitionFee(Request $request, $id)
    {
        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();

        $required_tuition_fee_amount = $student->program->tuitionFee->amount;

        // which registration
        $registration = UniversityRegistration::with('studentRegistration')->findOrFail($id);

        // tuition fee category
        $category = PaymentCategory::whereName('tuition-fee')->first();
        // get the unused tuition fee
        $tuitionFeePayments = Payment::whereUserId(auth()->id())
            ->whereCategory($category->id)
            ->whereExpired(false)
            ->get();

        // which tuition fee payment
        $studentTuitionFeePayment = StudentTuitionFeePayment::whereUniversityRegistrationId($registration->id)->first();


        // Find the paid
        $paid_amount = $studentTuitionFeePayment->paid_amount;
        //Find the required
        $required_amount = $studentTuitionFeePayment->required_amount;

        $balance = $tuitionFeePayments->sum('amount');

        // case i & ii : less & exact
        if ($balance <= $required_amount) {
            $studentTuitionFeePayment->increment('paid_amount', $balance);

            // mark as used
            $tuitionFeePayments->each(function ($tuitionFeePayment) {
                        $tuitionFeePayment->expired = true;
                        $tuitionFeePayment->save();
                    });

            if ($balance == $required_amount) {
                $studentTuitionFeePayment->completed = true;
                $studentTuitionFeePayment->save();                
            }
        }

        // case iii : more
        if ($balance > $required_amount) {
        
            // mark as complete
            $studentTuitionFeePayment->completed = true;
            $studentTuitionFeePayment->paid_amount = $required_amount;
            $studentTuitionFeePayment->save();

            // exceeded amount
            $execeeded = $balance - $required_amount;
            
            // make new transaction
            $payment = new Payment();
            $payment->amount = $execeeded;
            $payment->year_of_study = 1;
            $payment->semester = 'II';
            $payment->category = $category->id;
            $payment->user_id = auth()->id();
            $payment->date_of_payment = Carbon::now();
            $payment->receipt_number = str_random(30);
            $payment->save();

            // mark as used
            $tuitionFeePayments->each(function ($tuitionFeePayment) {
                        $tuitionFeePayment->expired = true;
                        $tuitionFeePayment->save();
                    });                        
        }

        return back();
    }

    public function directCost($id)
    {
        // Get required direct cost
        $requiredDirectCost = PaymentType::whereName('direct-cost')->first();

        $registration = UniversityRegistration::with('studentRegistration')->findOrFail($id);
        $studentDirectCostPayment = StudentDirectCostPayment::whereUniversityRegistrationId($registration->id)->firstOrCreate([
            'university_registration_id' => $registration->id,
            'user_id' => auth()->id(),
            'student_id' => auth()->user()->student->id,
            'required_amount' => $requiredDirectCost->amount,
        ]);
        // Get direct cost
        $category = PaymentCategory::whereName('direct-cost')->first();
        // Get the unused direct cost
        $directCostPayments = Payment::whereUserId(auth()->id())
            ->whereCategory($category->id)
            ->whereExpired(false)
            ->get();
        if ($studentDirectCostPayment) {
            $studentDirectCostPayment->required_amount = $requiredDirectCost->amount;

        }

        $student = auth()->user()->student()->with(['college', 'school', 'department', 'program'])->first();
        return view('registrations.partials.direct-cost', compact('registration', 'student', 'requiredDirectCost', 'directCostPayments', 'studentDirectCostPayment'));
    }

    public function payForDirectCost(Request $request, $id)
    {
        $registration = UniversityRegistration::with('studentRegistration')->findOrFail($id);

        $category = PaymentCategory::whereName('direct-cost')->first();
        $directCostPayments = Payment::whereUserId(auth()->id())
            ->whereCategory($category->id)
            ->whereExpired(false)
            ->get();

        $studentDirectCostPayment = StudentDirectCostPayment::where('university_registration_id', $registration->id)
            ->where('user_id', auth()->id())
            ->where('student_id', auth()->user()->student->id)
            ->first();

        // find paid
        $paid_amount = $studentDirectCostPayment->paid_amount;
        $required_amount = $studentDirectCostPayment->required_amount;

        // get balance
        $balance = $directCostPayments->sum('amount');

        // case i && ii : less & equal
        if ($balance <= $required_amount) {
            $studentDirectCostPayment->increment('paid_amount', $balance);

            // mark as used
            $directCostPayments->each(function ($directCostPayment) {
                $directCostPayment->expired = true;
                $directCostPayment->save();
            });            

            if ($balance == $required_amount) {
                $studentDirectCostPayment->completed = true;
                $studentDirectCostPayment->save();                
            }
        }



        // case iii : more
        if ($balance > $required_amount) {
            // mark as complete
            $studentDirectCostPayment->completed = true;
            $studentDirectCostPayment->paid_amount = $required_amount;
            $studentDirectCostPayment->save();

            // exceeded amount
            $execeeded = $balance - $required_amount;
            
            // make new transaction
            $payment = new Payment();
            $payment->amount = $execeeded;
            $payment->year_of_study = 1;
            $payment->semester = 'II';
            $payment->category = $category->id;
            $payment->user_id = auth()->id();
            $payment->date_of_payment = Carbon::now();
            $payment->receipt_number = str_random(30);
            $payment->save();

            // mark as used
            $directCostPayments->each(function ($directCostPayment) {
                        $directCostPayment->expired = true;
                        $directCostPayment->save();
                    });                        
        }


        if ($studentDirectCostPayment->paid_amount >= $studentDirectCostPayment->required_amount) {

        //student room assignment
  $user_id = Auth::user()->id;
  $studentgender = Student::where('user_id',$user_id)->first()->gender;
  $genderid=Gender::where('name',$studentgender)->first()->id;
  $blockwings = Genderwing::where('gender_id',$genderid)->get();
  foreach($blockwings as $blocks){
   $blockcollection[]=$blocks->block_id;
   $wingcollection[]=$blocks->wing_id;
  }
  $block =collect( $blockcollection)->random();
  if($block==null){
   $block=$blockwings->first()->block_id;
  }
  $wing =collect( $wingcollection)->random();
  
  $blocks = Block::whereId($block)->first();
  $wings = Wing::whereId($wing)->first();
  $roomnumbers = $wings->rooms->pluck('id');
 $room = collect($roomnumbers)->random();

 $accomodation = Accomodation::where('university_registrations_id',$registration->id)
                                        ->where('block_id',$blocks->id)->where('wing_id',$wings->id)
                                        ->where('room_id',$room)->get();
        $accNumber = $accomodation->count();
        if($accNumber<=4)
        {
            $accomodation = new Accomodation();
            $accomodation->university_registrations_id  = $registration->id;
            $accomodation->block_id = $blocks->id;
            $accomodation->wing_id=$wings->id;
            $accomodation->room_id = $room;
            $accomodation->student_id=Auth::User()->Student()->first()->id;
            $accomodation->save();
        }
        }

       return back();
    }

    public function preview()
    {
        $student = auth()->user()->student;
        return view('registrations.steps.step-two', compact('student'));
    }
}
