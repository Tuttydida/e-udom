<?php

namespace App\Http\Controllers\Staff;

use App\School;
use App\Department;
use App\Registration;
use App\UniversityRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    public function index()
    {
    	$registrations = collect([]);
    	
    	if(auth()->user()->hasRole('head-of-department')) {
	    	$department = Department::with('school')->whereHead(auth()->id())->first();
	    	$school = $department->school()->with('college')->first();
	    	$college = $school->college;

	    	
	    	$universityRegistrationId = UniversityRegistration::with('studentRegistration')
	    											->has('studentRegistration')
	    											->first()->id;
	    	$registrations = Registration::with('user')->where('university_registration_id', $universityRegistrationId)->get();    		
    	}

    	return view('staff.registrations.index', compact('registrations'));
    }
}
