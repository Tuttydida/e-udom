<?php

namespace App\Http\Controllers\Staff;

use App\Document;
use Carbon\Carbon;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class DocumentController extends Controller
{
    /**
     * Show all documents associated with staff
     * @return void
     */
    public function index()
    {
        $department = Department::whereHead(auth()->user()->id)->first();
        $documents = Document::with('user')->whereDepartmentId($department->id)->latest()->get();
        return view('staff.documents.index', compact('documents', 'department'));
    }

    public function show(Document $document)
    {
        return view('staff.documents.show', compact('document'));
    }

    public function approve(Request $request, Document $document)
    {
        $document->hod_state = $request->input('hod_state');
        $document->hod_comment = request('hod_comment');
        $document->save();

        return redirect('staff/documents');
    }

    public function read(Document $document)
    {
        dd('here');
        $document = storage_path('app/' . $document->path);

        return $document;
    }
}
