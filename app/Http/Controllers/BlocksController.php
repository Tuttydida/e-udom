<?php

namespace App\Http\Controllers;
use App\Block;
use App\Wing;
use App\Gender;
use Illuminate\Http\Request;
use App\Accomodation;
use App\Genderwing;
class BlocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $blocks = Block::all();
      return view('dean-of-student.accomodation.blocks.index',compact('blocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Block $block)
    {
        return view('dean-of-student.accomodation.blocks.create',compact($block));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request,[
             'name'=>'required',
            ]);
      
       $malegender = Gender::skip(0)->first();
        $femalegender = Gender::skip(1)->first();
        $wingA = Wing::skip(0)->first();
        $wingB = Wing::skip(1)->first();
        $wingA->gender()->attach($malegender);
          $id1 = Genderwing::where('wing_id',$wingA->id)->where('gender_id',$malegender->id)->latest()->first();
        $wingB->gender()->attach($femalegender);
        $id2 = Genderwing::where('wing_id',$wingB->id)->where('gender_id',$femalegender->id)->latest()->first();
        $blocks = new Block();
        $blocks->name=$request->input('name');
        $blocks->save();
        $genderwing1=Genderwing::find($id1);
       $genderwing2=Genderwing::find($id2);
        $genderwing1->block_id=$blocks->id;
        $genderwing2->block_id=$blocks->id;
        $genderwing1->save();
        $genderwing2->save();
       return redirect('dean-of-students/accomodation/blocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($blockId)
    { 
        $blocks = Block::whereId($blockId)->first();
        $wings = Wing::all();
       return view('dean-of-student.accomodation.blocks.show',compact('blocks','wings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
   public function blocks_constraints(){

          $blocks = Block::all(); 
          $genders = Gender::all(); 
       return view('dean-of-student.accomodation.blocks.block_contraints',compact('blocks','genders'));

    }
       public function wings_constraints(){

          $wings = Wing::all();
          $blocks = Block::all(); 
          $genders = Gender::all();   
       return view('dean-of-student.accomodation.blocks.wing_constraints',compact('blocks','wings','genders'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request,[
        'block'=>'required',
        'gender'=>'required',
        ]);

   $block = Block::whereid($request->input('block'))->first();
   $block->gender = $request->input('gender');
   $block->save();
   return redirect('dean-of-students/accomodation/blocks');
    }
   public function updatewings(Request $request)
    {
      $this->validate($request,[
        'block'=>'required',
        'wing'=>'required',
        'gender'=>'required',
        ]);
  $block=$request->input('block');
  $wing=$request->input('wing');
$genderwing = Genderwing::where('block_id',$block)->where('wing_id',$wing)->first();
  $genderwing->gender_id= $request->input('gender');
  $genderwing->save();
   return redirect('dean-of-students/accomodation/blocks');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
