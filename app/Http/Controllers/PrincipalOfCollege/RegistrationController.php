<?php

namespace App\Http\Controllers\PrincipalOfCollege;

use App\College;
use App\Registration;
use App\UniversityRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
 	public function index()
    {
    	$registrations = collect([]);
    	
    	if(auth()->user()->hasRole('principal-of-college')) {
	    	$college = College::wherePrincipal(auth()->id())->first();

	    	
	    	$universityRegistrationId = UniversityRegistration::with('studentRegistration')
	    											->has('studentRegistration')
	    											->first()->id;
	    	$registrations = Registration::with('user')->where('university_registration_id', $universityRegistrationId)->get();    		
	    }

	    return view('principal-of-college.registrations.index', compact('registrations'));
    }    
}
