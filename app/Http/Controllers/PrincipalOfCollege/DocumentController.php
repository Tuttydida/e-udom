<?php

namespace App\Http\Controllers\PrincipalOfCollege;

use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index()
    {
        $documents = Document::where('hod_state', 'approved')->get();
        return view('principal-of-college.documents.index', compact('documents'));
    }

    public function show($id)
    {
        $document = Document::where('hod_state', 'approved')->findOrFail($id);
        return view('principal-of-college.documents.show', compact('document'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'principal_state' => 'required',
        ]);

        $document = Document::where('hod_state', 'approved')->findOrFail($id);

        $document->principal_state = $request->input('principal_state');
        $document->principal_comment = $request->input('principal_comment');
        $document->save();

        return redirect('principal-of-college/documents');
    }
}
