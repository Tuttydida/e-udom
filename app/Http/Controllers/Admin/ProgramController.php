<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TuitionFeeType;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programs = Program::get();
        return view('admin.programs.index', compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::get();
        $tuition_fee_types = TuitionFeeType::get();
        return view('admin.programs.create', compact('departments', 'tuition_fee_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'department_id' => 'required'
        ]);

        $program = new Program();
        $program->name = request('name');
        $program->duration = request('duration');
        $program->department_id = request('department_id');
        $program->tuition_fee_type_id = request('tuition_fee_type_id');
        $program->save();

        return redirect('admin/programs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
     $program->delete();
        return redirect('/admin/programs');
    }
}
