<?php

namespace App\Http\Controllers\Admin;

use App\College;
use App\Department;
use App\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\User;
use App\Role;
use App\Program;
use App\CurrentStudentStatus;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::get();
        return view('admin.students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $programs = Program::get();

        return view('admin.students.create', compact('users', 'programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_file' => 'required',
            'program_id' => 'required',
        ]);

        $role = Role::where('name', 'student')->first();

        $program_id = request('program_id');
        $program = Program::find($program_id)->first();
        $department = Department::where('id', $program->department_id)->first();
        $school = School::find($department->school_id)->first();
        $college = College::find($school->college_id)->first();


        // load the rows
        $collections = \Excel::load($request->file('user_file'), function ($reader) {
        })->get();
        $count = 0;
        $is_sheets = 0;
        foreach ($collections as $collection) {
            foreach ($collection as $coll) {
                if (is_object($coll)) {
                    $is_sheets = 1;
                } else {
                    $is_sheets = 0;
                }
            }
        }

        if ($is_sheets == 1) {
            for ($i = 0; $i < 1; $i++) {
                foreach ($collections[$i] as $row) {
                    $user = new User();
                    $user->username = $row->regno;
                    $user->password = bcrypt('secret');
                    $user->save();
                    $student = new Student();
                    $student->user_id = $user->id;
                    $student->first_name = $row->firstname;
                    $student->middle_name = $row->middlename;
                    $student->last_name = $row->lastname;
                    $student->program_id = $program_id;
                    $student->department_id = $department->id;
                    $student->school_id = $school->id;
                    $student->college_id = $college->id;
                    $student->gender = $row->gender;
                    $student->nationality = $row->nationality;
                    $student->dob = $row->dob;
                    $student->save();
                    $user->roles()->attach($role);
                    $count++;
                }
            }
        }
        if ($is_sheets == 0) {
            foreach ($collections as $row) {
                $user = new User();
                $user->username = $row->regno;
                $user->password = bcrypt('secret');
                $user->save();
                $student = new Student();
                $student->user_id = $user->id;
                $student->first_name = $row->firstname;
                $student->middle_name = $row->middlename;
                $student->last_name = $row->lastname;
                $student->program_id = $program_id;
                $student->department_id = $department->id;
                $student->school_id = $school->id;
                $student->college_id = $college->id;
                $student->gender = $row->gender;
                $student->nationality = $row->nationality;
                $student->dob = $row->dob;
                $student->save();
                $user->roles()->attach($role);
                $count++;
            }
        }

        $currentStudentStatus = new CurrentStudentStatus();
        $currentStudentStatus->year_of_study_id = 1;
        $currentStudentStatus->semester_id = 1;
        $currentStudentStatus->student_id = $user->id;
        $currentStudentStatus->save();
        return redirect('admin/students');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect('/admin/students/');
    }
}
