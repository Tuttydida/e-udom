<?php

namespace App\Http\Controllers\Admin;

use App\College;
use Carbon\Carbon;
use App\Registration;
use Illuminate\Http\Request;
use App\UniversityRegistration;
use App\Http\Controllers\Controller;

class UniversityRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $universityRegistrations = UniversityRegistration::latest()->get();
        return view('admin.university-registrations.index', compact('universityRegistrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colleges = College::all();
        return view('admin.university-registrations.create', compact('colleges'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ur = new UniversityRegistration();
        $ur->academic_year = $request->input('academic_year');
        $ur->semester = $request->input('semester');
        $ur->starting_date = Carbon::now()->addWeek();
        $ur->ending_date = Carbon::now()->addWeek(2);
        $ur->status = $request->input('status');
        $ur->description = $request->input('description');
        $ur->college_id = $request->input('college_id');
        $ur->save();

        return redirect('admin/university-registrations');
    }

    /**
     * Display the specified resource.
     *
     * @param UniversityRegistration $universityRegistration
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($id)
    {
        $universityRegistration = UniversityRegistration::findOrFail($id); 
        $registrations = Registration::with('user')->latest()->get(); 
        return view('admin.university-registrations.show', compact('universityRegistration', 'registrations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
