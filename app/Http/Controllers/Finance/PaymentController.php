<?php

namespace App\Http\Controllers\Finance;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::with('student')->has('student')->latest()->get();

        return view('finances.payments.index', compact('payments'));
    }
}
