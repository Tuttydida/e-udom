<?php

namespace App\Http\Controllers\DeanOfSchool;

use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function index()
    {
        $documents = Document::where('hod_state', 'approved')->get();
        return view('dean-of-school.documents.index', compact('documents'));
    }

    public function show($id)
    {
        $document = Document::where('hod_state', 'approved')->findOrFail($id);
        return view('dean-of-school.documents.show', compact('document'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'deanS_state' => 'required',
        ]);

        $document = Document::where('hod_state', 'approved')->findOrFail($id);

        $document->deanS_state = $request->input('deanS_state');
        $document->deanS_comment = $request->input('deanS_comment');
        $document->save();

        return redirect('dean-of-school/documents');
    }
}
