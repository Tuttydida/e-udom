<?php

namespace App\Http\Controllers\DeanOfSchool;

use App\School;
use App\Registration;
use Illuminate\Http\Request;
use App\UniversityRegistration;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
 	public function index()
    {
    	$registrations = collect([]);
    	
    	if(auth()->user()->hasRole('dean-of-school')) {
	    	$school = School::whereDean(auth()->id())->first();
	    	$college = $school->college;

	    	
	    	$universityRegistrationId = UniversityRegistration::with('studentRegistration')
	    											->has('studentRegistration')
	    											->first()->id;
	    	$registrations = Registration::with('user')->where('university_registration_id', $universityRegistrationId)->get();    		
	    }

	    return view('dean-of-school.registrations.index', compact('registrations'));
    }    
}