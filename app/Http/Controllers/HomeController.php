<?php

namespace App\Http\Controllers;

use App\College;
use App\Department;
use App\School;
use Illuminate\Http\Request;
use App\UniversityRegistration;
use Auth;
use App\CurrentStudentStatus;
use App\Student;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $university_registrations = UniversityRegistration::all();

        $stats = [
            'colleges_count' => College::count(),
            'schools_count' => School::count(),
            'departments_count' => Department::count(),
            'students_count' => Student::count(),
        ];

        return view('home', compact('university_registrations', 'stats'));
    }
}
