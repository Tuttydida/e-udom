<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UniversityRegistration;
use App\Block;
use App\Wing;
use App\Room;
use App\Student;
use App\Gender;
use App\Genderwing;
use DB;
use App\Registration;
use App\Accomodation;
use Auth;
class AccomodationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

      $accomodation = Accomodation::where('student_id',auth()->user()->student->id)
                       ->where('university_registrations_id',$id)
                       ->with('block')->with('wing')->first();

          $direct_cost_status = DB::table('student_direct_cost_payments')
                                         ->select('completed')
                                          ->where('user_id', auth()->id())
                                            ->where('student_id', auth()->user()->student->id)
                                            ->where('completed', true)
                                            ->where('university_registration_id',$id)
                                            ->get();
      $registration = UniversityRegistration::findOrFail($id);
        $registration_status=Registration::find($id);
            if($registration_status==null){
                $regstatus=0;
            }else{
                $registration_status=$registration_status->where('user_id',auth()->user()->id)->first();
                if($registration_status==null){
                  $regstatus=1;  
                }
                $regstatus=1;
            }
        return view('registrations.accomodation.index',compact('registration','direct_cost_status','accomodation','regstatus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$accomodationId)
    {
  $user_id = Auth::user()->id;
  $studentgender = Student::where('user_id',$user_id)->first()->gender;
  $genderid=Gender::where('name',$studentgender)->first()->id;
  $blockwings = Genderwing::where('gender_id',$genderid)->get();
  foreach($blockwings as $blocks){
   $blockcollection[]=$blocks->block_id;
   $wingcollection[]=$blocks->wing_id;
  }
  $block =collect( $blockcollection);
  $wing =collect( $wingcollection);

  $blocks = Block::whereIn('id',$block)->get();
  $wings = Wing::whereIn('id',$wing)->get();
  $rooms = Room::all();

        $accomodation = Accomodation::whereId($accomodationId)->first();
        $registration = UniversityRegistration::findOrFail($id);
       return view('registrations.accomodation.create',compact('registration','blocks','wings','rooms','accomodation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$registrationId,$accomodationId)
    {
     $this->validate($request,[
        'block'=>'required|numeric',
        'wing'=>'required|numeric',
        'room'=>'required|numeric',
        ]);
           
           $accomodation = Accomodation::whereId($accomodationId)->first();
            $accomodation->block_id = $request->input('block');
            $accomodation->wing_id=$request->input('wing');
            $accomodation->room_id = $request->input('room');
            $accomodation->student_id=Auth::User()->Student()->first()->id;
            $accomodation->save();
           return redirect('registrations/'.$registrationId.'/accommodation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
