<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Block;
use App\Wing;
use App\Room;
use App\Genderwing;
use App\Gender;
class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($blockId,$wingId)
    {
       
  $blockwings = Genderwing::where('block_id',$blockId)->where('wing_id',$wingId)->first();
    $wing = Wing::whereid($blockwings->wing_id)->with('rooms')->first();
  $gender = Gender::find($blockwings->gender_id);
   $block = Block::find($blockId);
return view('dean-of-student.accomodation.rooms.index',compact('wing','gender','block'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($blockId,$wingzid)
    {
   
     return view('dean-of-student.accomodation.rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$blockId,$wingId)
    {
        $this->validate($request,[
            'room'=>'required|numeric',
            ]);
     $count=0;
     $rooms = Room::all();
     $block = Block::whereId($blockId)->first();
     $roomnum= Room::all()->count();
    $wing= Wing::whereId($wingId)->first();
     if($roomnum > 0 )
     {
     if($request->input('room')<=$roomnum){
      foreach ($rooms as $room) {
        if($count<$request->input('room'))
        {
            if($wing->whereHas('rooms')){
            $wing->rooms()->attach($room->id);
              $count++;
            }
        }
      }
     }
     }else{
        
         for($i=1;$i<=$request->input('room');$i++)
        {
          $room = new Room();
          $room->id=$i;
          $room->save();
          $wing->rooms()->attach($room);
        }
     }
     return redirect('dean-of-students/accomodation/block/'.$blockId.'/wing/'.$wingId.'/rooms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
