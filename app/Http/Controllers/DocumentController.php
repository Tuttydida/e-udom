<?php

namespace App\Http\Controllers;

use App\Notifications\DocumentSubmitted;
use App\User;
use Illuminate\Http\Request;
use App\Document;
use App\DocumentType;
use Auth;
use Illuminate\Support\Facades\Notification;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $documents = Auth::user()->documents()->latest()->get();
        $documentTypes = DocumentType::get();
        return view('documents.index', compact('documents', 'documentTypes'));
    }

    public function create()
    {
        $documentTypes = DocumentType::get();
        return view('documents.create', compact('documentTypes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'document_type_id' => 'required',
            'document' => 'required|max:1000|mimes:pdf',
        ]);

        $path = $request->document->store('documents');

        $document = new Document();
        $document->path = $path;
        $document->document_type_id = request('document_type_id');
        $document->user_id = Auth::user()->id;
        $document->department_id = auth()->user()->student->program->department->id;
        $document->save();

        $staff = User::whereIn('username', ['hod', 'deanS', 'principal'])->get();

        Notification::send($staff, new DocumentSubmitted($document));

        return redirect('documents');
    }

    public function show(Document $document)
    {
        $reply = $document->reply;
        return view('students.documents.show', compact('document', 'reply'));
    }
}
