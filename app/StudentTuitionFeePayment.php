<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentTuitionFeePayment extends Model
{
    protected $fillable = [
    	'university_registration_id',
    	'user_id',
    	'student_id', 
    	'required_amount',
    ];
}
