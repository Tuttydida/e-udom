<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentStudentStatus extends Model
{
    public function yearOfStudy()
    {
    	return $this->belongsTo('App\YearOfStudy', 'year_of_study_id');
    }
    
    public function student()
    {
        return $this->belongsTo('App\Student');
        
    
    }
}
