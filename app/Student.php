<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['user_id', 'program_id'];

    public function program()
    {
        return $this->belongsTo('App\Program');
    }

    public function school()
    {
        return $this->belongsTo('App\School');
    }

    public function college()
    {
        return $this->belongsTo('App\College');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function CurrentStudentStatus()
    {
        return $this->hasMany('App\CurrentStudentStatus');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment', 'user_id');
    }

    public function universityRegistrations()
    {
        return $this->belongsToMany('App\UniversityRegistration', 'registration_student', 'university_registration_id', 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function accomodations(){
        
        return $this->belongsToMany(Accomodation::class);
    }
}
