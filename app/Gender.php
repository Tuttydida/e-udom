<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
   public function wing()
   {
   	return $this->belongsToMany(Wing::class)->withPivot('block_id','id')->withTimestamps();
   }
}
