<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['hod_state', 'hod_comment', 'deanS_state', 'deanS_comment', 'principal_state', 'principal_comment'];

    protected $dates = ['approved_at'];

    public function documentType()
    {
    	return $this->belongsTo('App\DocumentType');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function department()
    {
    	return $this->belongsTo('App\Department');
    }

    public function reply()
    {
        return $this->hasOne('App\DocumentReply');
    }

    public function replies()
    {
        return $this->hasMany(DocumentReply::class, 'document_id');
    }

}
