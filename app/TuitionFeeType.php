<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TuitionFeeType extends Model
{
    protected $fillable = ['name', 'amount'];

    public function programs()
    {
    	return $this->hasMany('App\Program');
    }    
}
