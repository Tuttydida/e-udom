<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	public function getDates()
	{
		return [
			'date_of_payment'
		];
	}
    public function student()
    {
    	return $this->belongsTo(Student::class, 'user_id');
    }
}
