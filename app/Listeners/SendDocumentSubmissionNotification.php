<?php

namespace App\Listeners;

use App\Events\DocumentSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDocumentSubmissionNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DocumentSubmitted  $event
     * @return void
     */
    public function handle(DocumentSubmitted $event)
    {
        dd('Sending notifications for document ' . $event->document);
    }
}
