<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    protected $fillable = ['name'];

    public function universityRegistration()
    {
        return $this->hasOne('App\UniversityRegistration');
    }

    public function schools()
    {
        return $this->hasMany(School::class);
    }
}
