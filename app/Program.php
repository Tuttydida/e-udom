<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $fillable = ['name', 'duration'];

    public function students()
    {
    	return $this->hasMany(Student::class);
    }

    public function tuitionFee()
    {
    	return $this->belongsTo('App\TuitionFeeType', 'tuition_fee_type_id');
    }

    public function department()
    {
    	return $this->belongsTo(Department::class);
    }
}
