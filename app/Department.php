<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name', 'school_id'];

    public function programs()
    {
        return $this->hasMany(Program::class);
    }

    public function hod()
    {
    	return $this->belongsTo(User::class, 'head');
    }

    public function school()
    {
    	return $this->belongsTo(School::class);
    }
}
