<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityRegistration extends Model
{

    protected $dates = ['starting_date', 'ending_date'];

    public function colleges()
    {
        return $this->belongsTo('App\College');
    }

    public function studentRegistration()
    {
        return $this->hasOne(Registration::class, 'university_registration_id');
    }
}
