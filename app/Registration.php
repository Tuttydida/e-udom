<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    public function getDates()
    {
    	return [
    		'date_of_registration',
    	];
    }

    public function universityRegistration()
    {
    	return $this->belongsTo(UniversityRegistration::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
