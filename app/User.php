<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function student()
    {
        return $this->hasOne('App\Student');
    }

    public function documents()
    {
        return  $this->hasMany('App\Document');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'user_id');
    }    
    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }
    

}
