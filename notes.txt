# Notes

============
Registration
============

### Factors to be considered that a student is fully registered

- Payment of tuition fee
  - At least the half of the yearly payment
- Payment of direct cost
- Payment of other costs

 tuition_fee_types
/********/\*********\
| Name    | Amount  |
|---------|---------|
| Type 1  | 700000  |
| Type 2  | 1200000 |
| Type 3  | 1500000 |


 tuition_fees
|---------|----------------------|
| Program | tuition_fee_type_id  |
|---------|----------------------|
| BSc. CS | 2                    |
| BSc. TE | 3                    |
| BSc. SE | 3                    |


Simulation
i. Given that there is student - Done
ii. who belongs to a certain program - Done
iii. program - amount
iv. student pays a certain amount
v. check if she/he meets requirements

XAMPP

X - Cross Platform
A - Apache (Web server)
M - MySQL
P - PHP
P - Perl

LAMPP
L - Linux

WAMPP
W - Windows

MAMPP
M - Macintosh



DocumentTypes
-------------
1. Appeal
2. Permission
3. Other

Documents
-----------



=====
Roles
=====
- Admin

- Student

- HOD

- Dean of School

- Principal

- Dean of students

- Bursar

- Block



==============
File tracking
==============

## Paths

- Scenario 1

  A student is submitting an appeal

  Path

  student>hod
