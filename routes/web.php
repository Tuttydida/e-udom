<?php


Route::group(['namespace' => 'Finance'], function () {
    Route::get('finance/payments', 'PaymentController@index');
});

Route::get('help', 'PageController@help');

Route::get('register', function () {
    return 'register';
    return redirect('login');
});

Route::get('/', function () {
    return redirect('login');
});

Route::get('documents/{document}/read', function ($id) {
    $document = App\Document::findOrFail($id);

    $document = public_path('samples\sample.pdf');

    return response()->file($document);
});

Route::resource('registrations', 'RegistrationController');
Route::get('registrations/{registration}/student-details', 'RegistrationController@studentDetails');
Route::patch('registrations/{registration}/student-details', 'RegistrationController@updateStudentDetails');
Route::get('registrations/{registration}/contact-address', 'RegistrationController@contactAddress');
Route::patch('registrations/{registration}/contact-address', 'RegistrationController@updateContactAddress');
Route::get('registrations/{registration}/parent-or-guardian', 'RegistrationController@parentOrGuardian');
Route::patch('registrations/{registration}/parent-or-guardian', 'RegistrationController@updateParentOrGuardian');
Route::get('registrations/{registration}/photo', 'RegistrationController@photo');
Route::patch('registrations/{registration}/photo', 'RegistrationController@updatePhoto');
Route::get('registrations/{registration}/complete-registration', 'RegistrationController@completeRegistration');
Route::post('registrations/{registration}/complete-registration', 'RegistrationController@confirmRegistration');
Route::get('registrations/{registration}/tuition-fee', 'RegistrationController@tuitionFee');
Route::post('registrations/{registration}/tuition-fee', 'RegistrationController@payForTuitionFee');
Route::get('registrations/{registration}/direct-cost', 'RegistrationController@directCost');
Route::post('registrations/{registration}/direct-cost', 'RegistrationController@payForDirectCost');
Route::get('registrations/{registration}/preview', 'RegistrationController@preview');

Route::group(['middleware' => 'validateBackHistory'], function () {

    Auth::routes();

    Route::get('/home', 'HomeController@index');

    Route::get('registrations/{registration}/steps/step-one', function () {
        $student = auth()->user()->student;

        // Get the program tuition fee
        $tuitionFee = $student->program->tuitionFee->amount;
        // Get the student's sponsor amount
        $sponsorAmount = 1400000;

        $directCost = 169000;
        $otherCost = 20000;

        // Loop and sum all student's unused payments
        // $unusedPayments = $student->payments()->whereExpired(false)->get();
        // $sumOfUnusedPayments = $student->payments()->whereExpired(false)->get()->sum('amount');
        $unusedPayments = $student->payments;
        $sumOfUnusedPayments = $student->payments->sum('amount');

        // Mark the payments as expired
        // $unusedPayments->each(function($unusedPayment) {
        //     $unusedPayment->expired = true;
        //     $unusedPayment->save();
        // });

        $sumOfOutStandings = $tuitionFee + $directCost + $otherCost;

        $outstandings = collect([
            [
                'name' => 'Tuition Fee',
                'amount' => $tuitionFee,
                'paid' => $sumOfUnusedPayments - $tuitionFee,
                'remaining' => 0,
            ],
            [
                'name' => 'Direct Cost',
                'amount' => $directCost,
                'paid' => $sumOfUnusedPayments - $directCost,
                'remaining' => 0,
            ],
            [
                'name' => 'Other Costs',
                'amount' => $otherCost,
                'paid' => $sumOfUnusedPayments - $otherCost,
                'remaining' => 0,
            ]
        ]);

        return view('registrations.steps.step-one', compact('outstandings'));
    });

    Route::get('registrations/{registration}/steps/step-two', function () {
        $student = App\Student::first();
        return view('registrations.steps.step-two', compact('student'));
    });
    Route::get('registrations/{registration}/steps/step-three', function ($id) {
        $universityRegistration = App\UniversityRegistration::find($id);
        $student = App\Student::first();
        $student->universityRegistrations()->sync([$universityRegistration->id]);

        $universityRegistration->status = true;
        $universityRegistration->save();

        return view('registrations.steps.step-three');
    });

    Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'role:admin']], function () {
        Route::get('admin/overview', 'AdminController@overview');
        Route::resource('admin/roles', 'RoleController');
        Route::resource('admin/university-registrations', 'UniversityRegistrationController');
        Route::resource('admin/permissions', 'PermissionController');
        Route::resource('admin/colleges', 'CollegeController');
        Route::resource('admin/schools', 'SchoolController');
        Route::resource('admin/departments', 'DepartmentController');
        Route::resource('admin/programs', 'ProgramController');
        Route::resource('admin/courses', 'CourseController');
        Route::resource('admin/students', 'StudentController');
        Route::resource('admin/staff', 'StaffController');
        Route::post('admin/staff/{staff}/addRoles', 'StaffController@addRoles');
    });

    Route::group(['middleware'=>['auth','role:dean-of-students']],function(){

        Route::get('dean-of-students/accomodation/blocks','BlocksController@index');
        Route::get('dean-of-students/accomodation/blocks/create','BlocksController@create');


         Route::get('dean-of-students/accomodation/blocks/create/constraints','BlocksController@blocks_constraints');
         Route::get('dean-of-students/accomodation/wings/create/constraints','BlocksController@wings_constraints');
        
        Route::get('dean-of-students/accomodation/block/{block}/view','BlocksController@show');
        Route::post('dean-of-students/accomodation/block/store','BlocksController@store');
        Route::post('dean-of-students/accomodation/block/update','BlocksController@update');
         Route::post('dean-of-students/accomodation/block/update/wing','BlocksController@updatewings');
    Route::get('dean-of-students/accomodation/block/{block}/wing/{wing}/rooms','RoomsController@index');
    Route::get('dean-of-students/accomodation/block/{block}/wing/{wing}/room/create','RoomsController@create');
     Route::post('dean-of-students/accomodation/block/{block}/wing/{wing}/room/save','RoomsController@store');
    });

    Route::resource('payments', 'PaymentController');
    Route::resource('university-registrations', 'UniversityRegistrationController');
    Route::get('university-registrations/{university_registration}/my-payments', 'UniversityRegistrationController@myPayments');


    Route::get('/registrations/{registration}/years/{year}/semesters/{semester}/oustandings', 'RegistrationController@outstandings');

    Route::get('registrations/{registration}/accommodation','AccomodationController@index');
    Route::get('registrations/{registration}/accommodation/edit','AccomodationController@create');


    Route::get('registrations/{registration}/accommodation/{accommodation}/create','AccomodationController@create');
    Route::post('registrations/{registration}/accommodation/{accommodation}','AccomodationController@store');

    Route::resource('documents', 'DocumentController');

    Route::group(['namespace' => 'Staff', 'middleware' => ['auth', 'role:head-of-department']], function () {
        Route::get('staff/documents', 'DocumentController@index');
        Route::get('staff/documents/{document}', 'DocumentController@show');
        Route::patch('/staff/documents/{document}/approve', 'DocumentController@approve');
        Route::get('/staff/documents/{document}/read', 'DocumentController@read');
        Route::get('/staff/registrations', 'RegistrationController@index');
    });

    Route::group(['namespace' => 'DeanOfSchool', 'middleware' => ['auth', 'role:dean-of-school']], function () {
        Route::get('dean-of-school/documents', 'DocumentController@index');
        Route::get('dean-of-school/documents/{document}', 'DocumentController@show');
        Route::get('dean-of-school/registrations', 'RegistrationController@index');
        Route::patch('dean-of-school/documents/{document}', 'DocumentController@update');
    });

    Route::get('admin-officer/documents', 'AdminOfficerController@index');
    Route::get('admin-officer/documents/{document}', 'AdminOfficerController@show');
    Route::post('admin-officer/documents/{document}/reply', 'DocumentReplyController@store');


    Route::group(['namespace' => 'PrincipalOfCollege', 'middleware' => ['auth', 'role:principal-of-college']], function () {
        Route::get('principal-of-college/registrations', 'RegistrationController@index');
        Route::get('principal-of-college/documents', 'DocumentController@index');
        Route::get('principal-of-college/documents/{document}', 'DocumentController@show');
        Route::patch('principal-of-college/documents/{document}', 'DocumentController@update');
    });

    Route::get('profile', function () {
        $student = auth()->user()->student;
        return view('profile', compact('student'));
    })->middleware('auth');


});

Route::get('notifications', 'NotificationController@index')->name('notifications');
Route::delete('notifications', 'NotificationController@destroy')->name('notifications');

Route::post('reg-report/generate', 'ReportController@generateRegReport');
Route::get('download/{file}', 'DownloadController@index');
