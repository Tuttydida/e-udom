<?php

use App\Payment;
use App\PaymentCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('statistics/registrations', function () {
    // TODO Write a query to fetch this data
    return collect([
        ['college' => 'CIVE', 'total' => 567],
        ['college' => 'COED', 'total' => 4566],
        ['college' => 'COES', 'total' => 234],
        ['college' => 'CHSS', 'total' => 6888],
        ['college' => 'CNMS', 'total' => 345],
        ['college' => 'CHAS', 'total' => 289]
    ]);
});

Route::post('payments', function () {
    if (request('amount') == null) {
        return [
            'status' => 'error',
            'message' => 'You did not specify the amount'
        ];
    }
    if (request('year_of_study') == null) {
        return [
            'status' => 'error',
            'message' => 'You did not specify the year of study'
        ];
    }
    if (request('year_of_study') != 1 && request('year_of_study') != 2 && request('year_of_study') != 3 && request('year_of_study') != 4 && request('year_of_study') != 5) {
        return [
            'status' => 'error',
            'message' => 'Invalid year value. Valid year values are 1, 2, 3, 4 and 5',
        ];
    }
    if (request('semester') == null) {
        return [
            'status' => 'error',
            'message' => 'You did not specify the semester'
        ];
    }
    if (request('semester') != 'I' && request('semester') != 'II') {
        return [
            'status' => 'error',
            'message' => 'Invalid semester value. Valid semester values are I and II'
        ];
    }
    if (request('category') == null) {
        return [
            'status' => 'error',
            'message' => 'You did not specify the category of payment'
        ];
    }
    if (request('reg_no') == null) {
        return [
            'status' => 'error',
            'message' => 'You did not specify the registration number'
        ];
    }

    $user = User::whereUsername(request('reg_no'))->first();
    if ($user == null) {
        return [
            'status' => 'error',
            'message' => 'No student exists with that registration number'
        ];
    }

    $category = PaymentCategory::whereName(request('category'))->first();
    if ($category == null) {
        return [
            'status' => 'error',
            'message' => 'No such payment category exists in the database'
        ];
    }

    $payment = new Payment();
    $payment->amount = request('amount');
    $payment->year_of_study = request('year_of_study');
    $payment->semester = request('semester');
    $payment->category = $category->id;
    $payment->user_id = $user->id;
    $payment->date_of_payment = Carbon::now();
    $payment->receipt_number = str_random(30);
    $payment->save();

    return [
        'saved' => 'true'
    ];
});
