# E-UDOM

We computerise the most day to day manual activities which are done at UDOM.

## Modules

1. File Tracking System

2. Registration System

### File Tracking System

- Students can submit documents

- Respective staff can view, verify and reply to the documents 

### Registration System

- Students can register for an academic year

- Students pay before registering

- Bursar can view all the payments

- Student can preview the registration form

- Student can update personal details and other information

- Student can change his/her photo

- Student can view his/her payment outstandings

## Users

- Principal of college

  username: principal

- Dean of school

  username: deanS

- Head of department

  username: hod

- Dean of students

  username: deanSt

- Bursar

  username: bursar

- Warden

  username: warden

- Student

  username: registration number
