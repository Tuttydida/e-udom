fetch('/api/statistics/registrations')
    .then(function (response) {
        return response.json();
    })
    .then(function (ids) {
        var data = {
            labels: ids.map(function (id) {
                return id.college;
            }),
            data: ids.map(function (id) {
                return id.total;
            })
        };

        console.log(data);

        var data = {
            datasets: [{
                backgroundColor: [
                    'rgb(255, 99, 132)',
                    'rgb(67, 99, 132)',
                    'rgb(13, 99, 132)',
                    'rgb(135, 99, 132)',
                    'rgb(90, 99, 132)',
                    'rgb(45, 99, 132)',
                ],
                data: data.data
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: data.labels
        };

        var ctx = document.getElementById("myChart").getContext('2d');

        // For a pie chart
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: data
        });

    })


