<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->integer('document_type_id')->unsigned()->index();
            $table->timestamp('approved_at');
            $table->integer('user_id')->index();
            $table->integer('department_id')->unsigned()->index();
            $table->enum('hod_state', ['waiting', 'approved', 'denined'])->default('waiting');
            $table->text('hod_comment')->nullable();
            $table->enum('deanS_state', ['waiting', 'approved', 'denined'])->default('waiting');
            $table->text('deanS_comment')->nullable();
            $table->enum('principal_state', ['waiting', 'approved', 'denined'])->default('waiting');
            $table->text('principal_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
