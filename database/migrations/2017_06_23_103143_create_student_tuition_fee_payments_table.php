<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTuitionFeePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() 
    {
        Schema::create('student_tuition_fee_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('university_registration_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('student_id')->unsigned()->index();
            $table->double('required_amount')->default(0);
            $table->double('paid_amount')->default(0);
            $table->boolean('completed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_tuition_fee_payments');
    }
}
