<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('academic_year');
            $table->enum('semester', ['I', 'II']);
            $table->timestamp('starting_date')->default(Carbon::now());
            $table->timestamp('ending_date')->default(Carbon::now()->addMonth(1));
            $table->boolean('status')->default(false);
            $table->text('description')->nullable();

            $table->integer('college_id')->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_registrations');
    }
}
