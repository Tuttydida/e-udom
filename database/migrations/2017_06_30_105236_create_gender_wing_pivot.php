<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenderWingPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gender_wing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('block_id')->nullable();
            $table->integer('wing_id')->unsigned();
            $table->integer('gender_id')->unsigned();
            $table->foreign('wing_id')->references('id')->on('wings')->onDelete('cascade');
        $table->foreign('gender_id')->references('id')->on('genders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gender_wing');
    }
}
