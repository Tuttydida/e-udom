<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('program_id')->unsigned()->index();
            $table->integer('school_id')->unsigned()->index()->nullable();
            $table->integer('college_id')->unsigned()->index()->nullable();
            $table->integer('department_id')->unsigned()->index()->nullable();

            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('first_name')->nullable();

            $table->string('gender')->nullable();
            $table->string('nationality')->nullable();
            $table->string('dob');
            $table->integer('yos')->nullable();

            $table->string('f4_school')->nullable();
            $table->string('f4_index')->nullable();
            $table->integer('f4_year')->nullable();

            $table->string('f6_school')->nullable();
            $table->string('f6_index')->nullable();
            $table->integer('f6_year')->nullable();

            $table->string('mobile')->nullable();
            $table->string('email')->nullable();

            $table->string('bank_account_number')->nullable();
            $table->string('bank_name')->nullable();

            $table->string('parent_name')->nullable();
            $table->string('parent_relationship')->nullable();
            $table->string('parent_address')->nullable();
            $table->string('parent_mobile')->nullable();

            $table->string('photo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
