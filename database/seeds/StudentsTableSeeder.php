<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->delete();

        $studentRole = Role::whereName('student')->first();

        foreach (range(1, 9) as $index) {
            $user = User::create(['username' => 'T/UDOM/2013/0464' . $index, 'password' => bcrypt('secret')]);
            factory('App\Student')->create(['user_id' => $user->id]);
            $user->roles()->attach($studentRole);
        }
    }
}
