<?php

use App\PaymentCategory;
use Illuminate\Database\Seeder;

class PaymentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_categories')->delete();

        PaymentCategory::create(['name' => 'tuition-fee']);
        PaymentCategory::create(['name' => 'direct-cost']);
        PaymentCategory::create(['name' => 'quality-assurance']);
    }
}
