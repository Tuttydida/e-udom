<?php

use Illuminate\Database\Seeder;

class DocumentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentType::create(['name' => 'Appeal', 'slug' => 'appeal']);
        DocumentType::create(['name' => 'Postponement', 'slug' => 'postponement']);
        DocumentType::create(['name' => 'Permission', 'slug' => 'permission']);
    }
}
