<?php

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_types')->delete();

        PaymentType::create(['name' => 'direct-cost', 'amount' => 161000]);
    }
}
