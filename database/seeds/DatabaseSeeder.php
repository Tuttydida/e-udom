<?php

use App\DocumentType;
use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PaymentCategoriesTableSeeder::class);
        $this->call(PaymentTypesTableSeeder::class);
        $this->call(WingsTableSeeder::class);
        $this->call(GenderTableSeeder::class);

        /*=====================
            DO NOT DELETE
        =====================*/
        // Roles
        $adminRole = Role::create(['name' => 'admin']);
        $studentRole = Role::create(['name' => 'student']);
        $hodRole = Role::create(['name' => 'head-of-department']);
        $pocRole = Role::create(['name' => 'principal-of-college']);
        $dosRole = Role::create(['name' => 'dean-of-school']);
        $dostRole = Role::create(['name' => 'dean-of-students']);
        $bursarRole = Role::create(['name' => 'bursar']);
        $wardenRole = Role::create(['name' => 'warden']);
        $adminOfficerRole = Role::create(['name' => 'administrative-officer']);
        // Permissions
        $addUserPerm = Permission::create(['name' => 'add-user']);
        $approveDocumentPerm = Permission::create(['name' => 'approve-document']);
        // Attach permissions
        $adminRole->attachPermission($addUserPerm);
        $hodRole->attachPermission($approveDocumentPerm);

        // Document Types
        DocumentType::create(['name' => 'Appeal', 'slug' => 'appeal']);
        DocumentType::create(['name' => 'Postponement', 'slug' => 'postponement']);
        DocumentType::create(['name' => 'Permission', 'slug' => 'permission']);

        /*=====================
            UP TO HERE
        ======================*/



        $admin = factory('App\User')->create(['username' => 'admin']);
        $student = factory('App\User')->create(['username' => 'student']);
        factory('App\Student')->create(['user_id' => $student->id]);
        $head = factory('App\User')->create(['username' => 'hod']);
        $principal = factory('App\User')->create(['username' => 'principal']);
        $deanS = factory('App\User')->create(['username' => 'deanS']);
        $deanSt = factory('App\User')->create(['username' => 'deanSt']);
        $bursar = factory('App\User')->create(['username' => 'bursar']);
        $warden = factory('App\User')->create(['username' => 'warden']);
        $adminOfficer = factory('App\User')->create(['username' => 'adminOfficer']);

        // Attach Roles
        $admin->attachRole($adminRole);
        $student->attachRole($studentRole);
        $head->attachRole($hodRole);
        $principal->attachRole($pocRole);
        $deanS->attachRole($dosRole);
        $deanSt->attachRole($dostRole);
        $bursar->attachRole($bursarRole);
        $warden->attachRole($wardenRole);
        $adminOfficer->attachRole($adminOfficerRole);

        // College
        $college = factory('App\College')->create(['name' => 'College of Informatics and Virtual Education', 'principal' => $principal->id]);
        $college2 = factory('App\College')->create(['name' => 'College of Humanities', 'principal' => $principal->id]);
        $college3 = factory('App\College')->create(['name' => 'College of Social Sciences', 'principal' => $principal->id]);
        $college4 = factory('App\College')->create(['name' => 'College of Business and Law', 'principal' => $principal->id]);
        $college5 = factory('App\College')->create(['name' => 'College of Health Sciences', 'principal' => $principal->id]);
        $college6 = factory('App\College')->create(['name' => 'College of Earth Sciences', 'principal' => $principal->id]);
        $college7 = factory('App\College')->create(['name' => 'College of Natural Sciences', 'principal' => $principal->id]);
        $college8 = factory('App\College')->create(['name' => 'College of Education', 'principal' => $principal->id]);
        // School
        $school = factory('App\School')->create(['dean' => $deanS->id]);
        // Department
        $department = factory('App\Department')->create(['head' => $head->id]);
        // TuitionFeeType
        $tuitionFeeType = factory('App\TuitionFeeType')->create();
        // Program
        $program = factory('App\Program')->create([
            'department_id' => $department->id,
            'tuition_fee_type_id' => $tuitionFeeType->id
        ]);

        // Courses
        $courses = factory('App\Course', 5)->create();

        // Register a student
        $user = factory('App\User')->create();

        factory('App\Student')->create(['user_id' => $student->id]);
    }

}
