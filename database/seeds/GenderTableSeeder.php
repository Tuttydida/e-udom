<?php

use Illuminate\Database\Seeder;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('genders')->delete();
       DB::table('genders')->insert([
         'name'=>'MALE',
       	]);
       DB::table('genders')->insert([
         'name'=>'FEMALE',
       	]);
       DB::table('genders')->insert([
         'name'=>'BOTH',
       	]);
    }
}
