<?php

use Illuminate\Database\Seeder;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User')->create(['username' => 'admin']);
        factory('App\User')->create(['username' => 'hod']);
        factory('App\User')->create(['username' => 'principal']);
        factory('App\User')->create(['username' => 'deanS']);
        factory('App\User')->create(['username' => 'deanSt']);
        factory('App\User')->create(['username' => 'bursar']);
        factory('App\User')->create(['username' => 'warden']);
        factory('App\User')->create(['username' => 'adminOfficer']);
    }
}
