<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'student']);
        Role::create(['name' => 'head-of-department']);
        Role::create(['name' => 'principal-of-college']);
        Role::create(['name' => 'dean-of-school']);
        Role::create(['name' => 'dean-of-students']);
        Role::create(['name' => 'bursar']);
        Role::create(['name' => 'warden']);
        Role::create(['name' => 'administrative-officer']);
    }
}
