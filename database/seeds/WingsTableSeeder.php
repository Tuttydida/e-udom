<?php

use Illuminate\Database\Seeder;

class WingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('wings')->delete();
       DB::table('wings')->insert(['name'=>'Wing A']);
       DB::table('wings')->insert(['name'=>'Wing B']);

    }
}
