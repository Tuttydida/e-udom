<?php
use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->word,
        'email' => $faker->safeEmail,
        'password' => bcrypt('secret'),
    ];
});

$factory->define(App\Registration::class, function (Faker\Generator $faker) {

    return [
        'academic_year' => '20' . $faker->randomNumber(2) . '/20' . $faker->randomNumber(2),
        'year_of_study' => 2,
        'semester' => 'I',
        'registration_status' => true,
        'date_of_registration' => Carbon::now()->subMonth(),
        'university_registration_id' => 1,
        'user_id' => function () {
            return App\User::firstOrCreate(['username' => 'T/UDOM/2013/04640'])->id;
        },
    ];
});

$factory->define(App\DocumentType::class, function (Faker\Generator $faker) {
    $name = ucfirst($faker->unique()->word);
    $slug = str_slug($name, '-');

    return [
        'name' => $name,
        'slug' => $slug,
    ];
});

$factory->define(App\Document::class, function (Faker\Generator $faker) {
    return [
        'path' => 'documents/' . str_random(40) . '.pdf',
        'document_type_id' => function () {
            return factory('App\DocumentType')->create()->id;
        },
        'approved_at' => Carbon::now(),
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'department_id' => function () {
            return factory('App\Department')->create()->id;
        },
        'hod_state' => 'approved',
        'deanS_state' => 'approved',
        'principal_state' => 'approved',
    ];
});

$factory->define(App\DocumentReply::class, function (Faker\Generator $faker) {
    return [
        'document_id' => function () {
            return factory('App\Document')->create()->id;
        },
        'path' => str_random(40),
        'description' => $faker->paragraph,
        'user_id' => function() {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(App\Student::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'program_id' => function () {
            return factory('App\Program')->create()->id;
        },
        'school_id' => function () {
            return factory('App\School')->create()->id;
        },
        'college_id' => function () {
            return factory('App\College')->create()->id;
        },
        'department_id' => function () {
            return factory('App\Department')->create()->id;
        },
        'last_name' => $faker->lastName,
        'middle_name' => $faker->firstName,
        'first_name' => $faker->firstName,
        'gender' => 'FEMALE',
        'nationality' => $faker->country,
        'dob' => \Carbon\Carbon::now()->subYear(17),
        'yos' => $faker->randomDigit,
        'f4_school' => $faker->sentence,
        'f4_index' => 'S.0155.0044',
        'f4_year' => '200' . $faker->randomDigit,
        'f6_school' => $faker->sentence,
        'f6_index' => 'S.0155.0056',
        'f6_year' => '200' . $faker->randomDigit,
        'mobile' => $faker->phoneNumber,
        'email' => $faker->email,
        'bank_account_number' => $faker->creditCardNumber,
        'bank_name' => $faker->creditCardType,

        'parent_name' => $faker->name,
        'parent_relationship' => 'parent',
        'parent_address' => $faker->address,
        'parent_mobile' => $faker->phoneNumber,
    ];
});

$factory->define(App\Payment::class, function (Faker\Generator $faker) {
    return [
        'amount' => $faker->randomNumber(4) . '000',
        'receipt_number' => str_random(14),
        'date_of_payment' => Carbon::now(),
        'year_of_study' => $faker->randomNumber(1),
        'semester' => 'I',
        'category' => function () {
            return factory('App\TuitionFeeType')->create()->id;
        },
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(App\TuitionFeeType::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word),
        'amount' => $faker->randomNumber(4) . '000',
    ];
});

$factory->define(App\Block::class, function (Faker\Generator $faker) {
    return [
        'name' => "Block-".$faker->numberBetween(1,6),
    ];
});
$factory->define(App\Room::class, function (Faker\Generator $faker) {
    return [
        'name' =>$faker->numberBetween(1,400),
    ];
});

$factory->define(App\Program::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'duration' => $faker->randomDigit(1),
        'department_id' => function () {
            return factory('App\Department')->create()->id;
        },
        'tuition_fee_type_id' => function () {
            return factory('App\TuitionFeeType')->create()->id;
        },
    ];
});

$factory->define(App\Department::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'head' => function () {
            return factory('App\User')->create()->id;
        },
        'school_id' => function () {
            return factory('App\School')->create()->id;
        },
    ];
});

$factory->define(App\School::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'dean' => function () {
            return factory('App\User')->create()->id;
        },
        'college_id' => function () {
            return factory('App\College')->create()->id;
        },
    ];
});

$factory->define(App\College::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'principal' => function () {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(App\Course::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'credits' => $faker->randomDigit(1),
    ];
});

$factory->define(App\UniversityRegistration::class, function (Faker\Generator $faker) {
    return [
        'academic_year' => '20' . $faker->randomNumber(2) . '/20' . $faker->randomNumber(2),
        'semester' => 'I',
        'starting_date' => Carbon::now()->addDay(),
        'ending_date' => Carbon::now()->addMonth(),
        'description' => $faker->paragraph,
        'college_id' => function () {
            return factory('App\College')->create()->id;
        },
    ];
});
