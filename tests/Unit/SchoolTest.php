<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SchoolTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_if_school_has_departments()
    {
        $school = factory('App\School')->create();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $school->departments);
    }
}
