<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CollegeTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_college_has_schools()
    {
        $college = factory('App\College')->create();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $college->schools);
    }
}
