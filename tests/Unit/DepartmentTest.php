<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DepartmentTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_if_a_department_has_programs()
    {
        $department = factory('App\Department')->create();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $department->programs);
    }
}
