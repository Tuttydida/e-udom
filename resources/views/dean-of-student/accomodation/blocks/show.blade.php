@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')           
        </div>        
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $blocks->name}}</div>
                <div class="panel-body">
                @if($blocks->count() >0 )
                <ul>
                @foreach($wings as $wing)
  <li> <a href="/dean-of-students/accomodation/block/{{$blocks->id}}/wing/{{$wing->id}}/rooms"> {{$wing->name}}</a></li>
                @endforeach
                </ul>
                @else
                @endif
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection