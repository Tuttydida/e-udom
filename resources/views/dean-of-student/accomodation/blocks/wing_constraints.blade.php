@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New block</h3>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')

                        <form style="width:800px;" action="/dean-of-students/accomodation/block/update/wing" method="POST" class="form-horizontal"
                              role="form">
                            {{ csrf_field() }}

                          <div class="form-group">
                               <label for="block" class="col-sm-2 col-sm-offset-2">Block</label>
                                <div class="col-sm-4 ">
                                  <select name="block" id="block" class="form-control" required="required">
                             @foreach($blocks as $block)
                              <option value="{{$block->id}}">Block-{{$block->name}}</option>
                             @endforeach
                             </select>
                                </div> 
                           </div>
                           <div class="form-group">
                               <label for="wing" class="col-sm-2 col-sm-offset-2">Wing</label>
                                <div class="col-sm-4 ">
                                  <select name="wing" id="wing" class="form-control" required="required">
                             @foreach($wings as $wing)
                              <option value="{{$wing->id}}">{{$wing->name}}</option>
                             @endforeach
                             </select>
                                </div> 
                           </div>
                           <div class="form-group">
                               <label for="gender" class="col-sm-2 col-sm-offset-2">Select gender</label>
                               <div class="col-sm-4 ">
                                <select name="gender" id="gender" class="form-control" required="required">
                                 @foreach($genders as $gender)
                                  <option value="{{$gender->id}}">{{$gender->name}}</option>
                                     @endforeach
                                </select>
                               </div>
                           </div>
                                   <div class="form-group">
                               <div class="col-sm-4 col-sm-offset-4">
                                  <button type="submit" class="btn btn-primary">Save</button>
                               </div>
                           </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
