@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Building blocks in the college of informatics and virtual education</div>
                    <div class="panel-body">
                    <a href="/dean-of-students/accomodation/blocks/create" class="btn btn-primary">New</a>
                        @if($blocks->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Block name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($blocks as $block)
                                     <tr>
                                        <td>{{ $block->id }}.</td>
                                        <td>
                                        <a href="/dean-of-students/accomodation/block/{{$block->id }}/view">
                                            Block-{{ $block->name }}  
                                        </a> 
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No blocks available yet.Click new to add new block 
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
