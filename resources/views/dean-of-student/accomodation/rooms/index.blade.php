@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Available rooms</div>
                    <div class="panel-body">
                      @if($wing->rooms->count() <= 0)
                        <a href="room/create" class="btn btn-primary">New</a><br><br>
                      @endif
                        @if($wing->rooms->count() >0)
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Side</th>
                                    <th>gender</th>
                                    <th>Room display name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($wing->rooms as $room)
                                     <tr>
                                       <td>{{$room->id}}</td>
                                       <td>{{$wing->name}}</td>
                                       <th>{{$gender->name}}</th>
                                        <td>{{$block->name}}-{{$room->id}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No rooms available yet.Click new to add new Rooms 
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
