@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Rooms</h3>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')
                        <form style="width:800px;" action="save" method="POST" class="form-horizontal"
                              role="form">
                            {{ csrf_field() }}
                           <div class="form-group">
                               <label for="name" class="col-sm-6">Enter the number of rooms per Wing</label>
                               <div class="col-sm-4 ">
                                   <input type="text" class="form-control" id="room" name="room">
                               </div>
                           </div>
                                   <div class="form-group">
                               <div class="col-sm-4 col-sm-offset-6">
                                  <button type="submit" class="btn btn-primary">Save</button>
                               </div>
                           </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
