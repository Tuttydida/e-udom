@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $document->documentType->name }}</div>

                    <div class="panel-body">
                        <a href="/documents/{{ $document->id }}/read" target="_blank">
                            <i class="fa fa-file-pdf-o fa-2x"></i>
                        </a>

                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Head of Department Comments</h3>
                    </div>
                    <div class="panel-body">
                        {{ $document->hod_comment }}
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Action</h3>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="/dean-of-school/documents/{{ $document->id }}">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="radio">
                                <label>
                                    <input type="radio" name="deanS_state" value="approved"> Approve
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="deanS_state" value="denined" checked="checked"> Disapprove
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="deanS_comment">Comment (Option)</label>
                                <textarea name="deanS_comment" id="deanS_comment" rows="5" class="form-control"
                                          placeholder="Write something..."></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
