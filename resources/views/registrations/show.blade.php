@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                @include('partials.sidebar')
            </div>
            <div class="col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }}
                            Semester: {{ $registration->semester }}
                            From: {{ $registration->starting_date->toFormattedDateString() }}
                            To: {{ $registration->ending_date->toFormattedDateString() }}
                            Opened: {{ $registration->created_at->diffForHumans() }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Notice</h3>
                                {{ $registration->description }}

                                <br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                @include('registrations.sidebar')
                            </div>
                            <div class="col-xs-9">

                                <h3>Your registration status</h3>

                                @if($registered=='not registered')

                                    @if($problems->count())
                                        <div class="alert alert-danger">
                                            <h4>You can not complete this registration until the following issues are
                                                fixed</h4>
                                            <ul>
                                                @foreach($problems as $problem)
                                                    <li>{{ $problem }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @else
                                        <div class="alert alert-success">
                                            <p class="lead">You are good to go!</p>
                                        </div>
                                        <a href="/registrations/{{ $registration->id }}/complete-registration"
                                           class="btn btn-success btn-lg btn-block">Complete the Registration</a>
                                    @endif

                                @else

                                    <div class="alert alert-success">
                                        <p class="lead">
                                            You are fully registered
                                        </p>
                                    </div>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
