@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Academic Year
                        &nbsp;
                        {{ $university_registration->academic_year }}
                        &nbsp;
                        Semester
                        &nbsp;
                        {{ $university_registration->semester }}
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <th>Tuition Fee Paid (Tshs)</th>
                                <td>{{ $tuition_fee_paid }}</td>
                            </tr>
                            <tr>
                                <th>Tuition Fee Remaining (Tshs)</th>
                                <td>{{ $outstanding }}</td>
                            </tr>
                            <tr>
                                <th>Direct Costs (Tshs)</th>
                                <td>0</td>
                            </tr>
                            <tr>
                                <th>Other</th>
                                <td>0</td>
                            </tr>
                            </tbody>
                        </table>

                        <a href="#" class="btn btn-success btn-block">Go to the registration form</a>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('partials.sidebar')
            </div>
        </div>
    </div>
@endsection
