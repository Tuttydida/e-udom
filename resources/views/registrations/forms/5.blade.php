		<div class="row">
			<div class="col-xs-12">
				<table class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="4">Parent/Guardian</th>
						</tr>
						<tr>
							<td>Name:</td>
							<td>
								{{ $student->parent_name }}
							</td>
							<td>Relationship</td>
							<td>
								{{ $student->parent_relationship }}
							</td>
						</tr>
						<tr>
							<td>Address</td>
							<td>
								{{ $student->parent_address }}
							</td>
							<td>Mobile</td>
							<td>
								{{ $student->parent_mobile }}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>