		<div class="row">
			<div class="col-xs-12">
				<table class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="2">Contact Address</th>
							<td colspan="2"><i>*Whichever is applicable</i></td>
						</tr>
						<tr>
							<td>Mobile:</td>
							<td>
								<{{ $student->mobile }}
							</td>
							<td>Email:</td>
							<td>
								{{ $student->email }}
							</td>
						</tr>
						<tr>
							<td>BANK Account Number</td>
							<td>
								{{ $student->bank_account_number }}
							</td>
							<td>BANK</td>
							<td>
								{{ $student->bank_name }}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>