<div class="row">
    <div class="col-xs-2 text-center">
        <img src="/img/logo.png" class="img-responsive" alt="">
    </div>
    <div class="col-xs-8 text-center">
        <strong>STUDENT REGISTRATION FORM 2016/2017</strong>
        <br>
        (CONTINUING STUDENTS)
        <br>
        <strong>SEMESTER i - November 2016</strong>
    </div>
    <div class="col-xs-2">
        <img src="/storage/{{ $student->photo }}" class="img-responsive" alt="">
    </div>
</div>