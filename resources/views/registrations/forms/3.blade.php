		<div class="row">
			<div class="col-xs-12">
				<table class="table table-bordered table-condensed">
					<tbody>
						<tr>
							<th colspan="6">Students Details</th>
						</tr>
 						<tr>
							<th>Programme:</th>
							<td></td>
							<th>School:</th>
							<td></td>
							<th>College:</th>
							<td></td>
						</tr>
						<tr>
							<th>Department</th>
							<td colspan="5"> 
								{{ $student->department_id }}
							</td>
						</tr>
						<tr>
							<td>
								<strong>Last Name/Surname</strong>
								<div class="small">
									(As in your certificates)
								</div>
							</td>
							<td>
								<strong>Other Names</strong>
								<div class="small">
									(Start with First Name)
								</div>
							</td>
							<td>
								<strong>
									Sex <br>
									(F/M)
								</strong>
							</td>
							<th>
								Nationality
							</th>
							<td>
								<strong>Date of Birth</strong>
								<br>
								<div class="small">
									(DD/MM/YEAR)
								</div>
							</td>
							<th>
								Year of Study
							</th>
						</tr>
						<tr>
							<td>{{ $student->last_name }}</td>
							<td>{{ $student->first_name }} {{ $student->middle_name }}</td>
							<td>{{ $student->gender }}</td>
							<td>{{ $student->nationality }}</td>
							<td>{{ $student->dob }}</td>
							<td>{{ $student->yos }}</td>
						</tr>
						<tr>
							<th>O Level School</th>
							<th>F4 Index Number</th>
							<th>Year</th>
							<th>*A Level School/Diploma College</th>
							<th>*F6/Diploma Index Number</th>
							<th>Year</th>
						</tr>
						<tr>
							<td>
								{{ $student->f4_school }}
							</td>
							<td>
								{{ $student->f4_index }}
							</td>
							<td>
								{{ $student->f4_year }}
							</td>
							<td>
								{{ $student->f6_school }}
							</td>
							<td>
								{{ $student->f6_index }}
							</td>
							<td>
								{{ $student->f6_year }}
							</td>
						</tr> 
					</tbody>
				</table>
			</div>
		</div>