<div class="list-group">
    <a href="/registrations/{{ $registration->id }}"
       class="list-group-item {{ Request::segment(3) == '' ? 'active' : 'false' }}">Home</a>
    <a href="/registrations/{{ $registration->id }}/student-details"
       class="list-group-item {{ Request::segment(3) == 'student-details' ? 'active' : 'false' }}">My Details</a>
    <a href="/registrations/{{ $registration->id }}/contact-address"
       class="list-group-item  {{ Request::segment(3) == 'contact-address' ? 'active' : 'false' }}">Contact
        Address</a>
    <a href="/registrations/{{ $registration->id }}/parent-or-guardian"
       class="list-group-item  {{ Request::segment(3) == 'parent-or-guardian' ? 'active' : 'false' }}">Parent Or
        Guardian</a>
    <a href="/registrations/{{ $registration->id }}/tuition-fee"
       class="list-group-item  {{ Request::segment(3) == 'tuition-fee' ? 'active' : 'false' }}">Tuition
        Fee</a>
    <a href="/registrations/{{ $registration->id }}/direct-cost"
       class="list-group-item  {{ Request::segment(3) == 'direct-cost' ? 'active' : 'false' }}">Direct
        Cost</a>
    <a href="/registrations/{{ $registration->id }}/accommodation"
       class="list-group-item  {{ Request::segment(3) == 'accommodation' ? 'active' : 'false' }}">Accommodation</a>
    <a href="/registrations/{{ $registration->id }}/photo" class="list-group-item  {{ Request::segment(3) == 'photo' ? 'active' : 'false' }}">Photo</a>
</div>