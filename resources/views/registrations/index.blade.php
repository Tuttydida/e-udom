@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">University Registrations</h3>
                    </div>
                    <div class="panel-body">
                        @if($openUniversityRegistrations->count())
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Academic Year</th>
                                    <th>Semester</th>
                                    <th>starting_date</th>
                                    <th>ending_date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($openUniversityRegistrations as $openUniversityRegistration)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $openUniversityRegistration->academic_year }}</td>
                                        <td>{{ $openUniversityRegistration->semester }}</td>
                                        <td>{{ $openUniversityRegistration->starting_date->toFormattedDateString() }}</td>
                                        <td>{{ $openUniversityRegistration->ending_date->toFormattedDateString() }}</td>
                                        <td>
                                            @if($openUniversityRegistration->status == false)
                                                <a href="/registrations/{{ $openUniversityRegistration->id }}"
                                                   class="btn btn-primary btn-sm">Perform</a>
                                            @else
                                                <span class="text-success"><strong>Registered</strong></span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                There are currently no open Registrations
                            </div>
                        @endif
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Your Registrations</div>

                    <div class="panel-body">
                        @if($registrations->count())
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Academic Year</th>
                                    <th>Year of Study</th>
                                    <th>Semester</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                @foreach($registrations as $registration)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $registration->academic_year }}</td>
                                        <td>{{ $registration->year_of_study }}</td>
                                        <td>{{ $registration->semester }}</td>
                                        <td>{{ $registration->date_of_registration->toFormattedDateString() }}</td>
                                        <td>
                                            @if($registration->registration_status)
                                                <span class="badge">Registered</span>
                                            @else
                                                <span class="badge">Not Registered</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h2>You have performed {{ $total_regs }} registration(s).</h2>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
