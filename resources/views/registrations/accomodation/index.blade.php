@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-2">
            @include('partials.sidebar')
        </div>        
        <div class="col-xs-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }} 
                            Semester: {{ $registration->semester }} 
                            From: {{ $registration->starting_date->toFormattedDateString() }} 
                            To: {{ $registration->ending_date->toFormattedDateString() }} 
                            Opened: {{ $registration->created_at->diffForHumans() }} 
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                          <h3 class="text-center">Accommodation details</h3> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            @include('registrations.sidebar')
                        </div>
                      @if($direct_cost_status->count()>0)
                                     <div class="col-xs-9">
                        <h3>Nature of accomodation:On campus</h3>
                       <table class="table table-bordered table-hover">
                           <thead>
                               <tr>
                                   <th>Block</th>
                                   <th>Wing</th>
                                   <th>Room</th>
                                   @if($regstatus==0)
                                     <th>Action</th>
                                   @endif
                               </tr>
                           </thead>
                           <tbody>
                               <tr>
                                   <td>
                                  
                                    {{   $accomodation->block->name }}
                        
                                   </td>
                                   <td>
                                    {{ $accomodation->wing->name }}
                                   </td>
                                   <td>
                                   {{ $accomodation->room->id }}
                                   </td>
                                     @if($regstatus==0)
                                   <td>
                                   <a href="accommodation/{{$accomodation->id}}/create" ><i class="fa fa-edit"></i>Change</a>
                                   </td>
                                   @endif
                               </tr>
                           </tbody>
                       </table>
                        </div>
                      @else
                       <div class="col-xs-9">
                             <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Note!</strong>You are not completed the direct cost payments, please make sure that you pay first.
                      </div>
                          </div>
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-2">
            @include('partials.sidebar')
        </div>        
        <div class="col-xs-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                            Academic Yeard: {{ $registration->academic_year }} 
                            Semesterd: {{ $registration->semester }} 
                            From: {{ $registration->starting_date->toFormattedDateString() }} 
                            Tod: {{ $registration->ending_date->toFormattedDateString() }} 
                            Opened: {{ $registration->created_at->diffForHumans() }} 
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Notice</h3>
                            {{ $registration->description }}

                            <br><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            @include('registrations.sidebar')
                        </div>
                        <div class="col-xs-9">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
