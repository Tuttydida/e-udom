@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-2">
            @include('partials.sidebar')
        </div>        
        <div class="col-xs-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                @include('errors.list')
                    <h3 class="panel-title">
                            Academic Yeard: {{ $registration->academic_year }} 
                            Semesterd: {{ $registration->semester }} 
                            From: {{ $registration->starting_date->toFormattedDateString() }} 
                            Tod: {{ $registration->ending_date->toFormattedDateString() }} 
                            Opened: {{ $registration->created_at->diffForHumans() }} 
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                          <h3 class="text-center">Accommodation details</h3> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            @include('registrations.sidebar')
                        </div>
                        <div class="col-xs-4 col-xs-offset-1">
                       <form  action="/registrations/{{$registration->id}}/accommodation/{{$accomodation->id}}" method="POST" role="form">
                       {{csrf_field()}}
                           <legend>Change room</legend>
                       
                           <div class="form-group">
                               <label for="block">Block</label>
                             <select name="block" id="block" class="form-control" required="required">
                             @foreach($blocks as $block)
                              <option value="{{$block->id}}">Block-{{$block->name}}</option>
                             @endforeach
                             </select>
                           </div>
                       
                           <div class="form-group">
                               <label for="Wing">Wing</label>
                               <select name="wing" id="wing" class="form-control" required="required">
                                  @foreach($wings as $wing)
                              <option value="{{$wing->id}}">{{$wing->name}}</option>
                             @endforeach
                               </select>
                           </div>

                           <div class="form-group">
                               <label for="room">Room number</label>
                               <select name="room" id="room" class="form-control" required="required">
                                   @foreach($rooms as $room)
                              <option value="{{$room->id}}">{{$room->id}}</option>
                             @endforeach
                               </select>
                           </div>
                       
                           <button type="submit" class="btn btn-primary">Submit</button>
                       </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-2">
            @include('partials.sidebar')
        </div>        
        <div class="col-xs-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                            Academic Yeard: {{ $registration->academic_year }} 
                            Semesterd: {{ $registration->semester }} 
                            From: {{ $registration->starting_date->toFormattedDateString() }} 
                            Tod: {{ $registration->ending_date->toFormattedDateString() }} 
                            Opened: {{ $registration->created_at->diffForHumans() }} 
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Notice</h3>
                            {{ $registration->description }}

                            <br><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            @include('registrations.sidebar')
                        </div>
                        <div class="col-xs-9">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
