@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                @include('partials.sidebar')
            </div>
            <div class="col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }}
                            Semester: {{ $registration->semester }}
                            From: {{ $registration->starting_date->toFormattedDateString() }}
                            Tod: {{ $registration->ending_date->toFormattedDateString() }}
                            Opened: {{ $registration->created_at->diffForHumans() }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Notice</h3>
                                {{ $registration->description }}

                                <br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                @include('registrations.sidebar')
                            </div>
                            <div class="col-xs-9">
                                <h3>Contact Address</h3>

                                @include('errors.list')

                                <form method="POST" action="/registrations/{{ $registration->id }}/contact-address">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}

                                    <input type="hidden" name="student_id" value="{{ $student->id }}">

                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                        <tr>
                                            <th colspan="2">Contact Address</th>
                                            <td colspan="2"><i>*Whichever is applicable</i></td>
                                        </tr>
                                        <tr>
                                            <td>Mobile:</td>
                                            <td>
                                                <input type="tel" name="mobile" value="{{ $student->mobile }}"
                                                       class="form-control">
                                            </td>
                                            <td>Email:</td>
                                            <td>
                                                <input type="email" name="email" value="{{ $student->email }}"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>BANK Account Number</td>
                                            <td>
                                                <input type="text" name="bank_account_number"
                                                       value="{{ $student->bank_account_number }}"
                                                       class="form-control">
                                            </td>
                                            <td>BANK</td>
                                            <td>
                                                <input type="text" name="bank_name" value="{{ $student->bank_name }}"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <button type="submit" class="btn btn-primary">Update</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
