@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                @include('partials.sidebar')
            </div>
            <div class="col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }}
                            Semester: {{ $registration->semester }}
                            From: {{ $registration->starting_date->toFormattedDateString() }}
                            Tod: {{ $registration->ending_date->toFormattedDateString() }}
                            Opened: {{ $registration->created_at->diffForHumans() }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Notice</h3>
                                {{ $registration->description }}

                                <br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                @include('registrations.sidebar')
                            </div>
                            <div class="col-xs-9">


                                @if($registration->studentRegistration == null)
                                    <h3>Complete My Registration</h3>
                                    <p>
                                        Before you complete your registration, please make sure that you have filled
                                        correct
                                        information.
                                    </p>

                                    <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Complete my
                                        Registration</a>
                                    <div class="modal fade" id="modal-id">
                                        <div class="modal-dialog">
                                            <form method="POST"
                                                  action="/registrations/{{ $registration->id }}/complete-registration">
                                                {{ csrf_field() }}

                                                <input type="hidden" name="student_id" value="{{ $student->id }}">

                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title">Complete Registration</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <p>
                                                            Are you sure that you want to complete this registration?
                                                        </p>
                                                        <p>
                                                            Once you say Yes, you can no longer edit the information you
                                                            submitted here.
                                                        </p>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">
                                                            No
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                @else

                                    <div class="alert alert-success">
                                        <h1>Your registration is complete</h1>
                                    </div>

                                    <a href="/registrations/{{ $registration->id }}/preview"
                                       target="_blank" class="btn btn-primary btn-lg btn-block">Preview
                                        & Download the Registration form</a>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
