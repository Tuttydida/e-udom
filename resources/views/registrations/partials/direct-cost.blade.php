@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                @include('partials.sidebar')
            </div>
            <div class="col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }}
                            Semester: {{ $registration->semester }}
                            From: {{ $registration->starting_date->toFormattedDateString() }}
                            Tod: {{ $registration->ending_date->toFormattedDateString() }}
                            Opened: {{ $registration->created_at->diffForHumans() }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Notice</h3>
                                {{ $registration->description }}

                                <br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                @include('registrations.sidebar')
                            </div>
                            <div class="col-xs-9">
                                <h3>Direct Cost</h3>
                                <div class="text-right">
                                    <div class="well">
                                        You have Tshs <strong>{{ number_format($directCostPayments->sum('amount')) }}</strong> in your account
                                    </div>
                                </div>

                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Required</th>
                                        <th>Paid</th>
                                        <th>Remained</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-right">
                                            {{ number_format($requiredDirectCost->amount) }}
                                        </td>
                                        <td class="text-right">
                                            {{ number_format($studentDirectCostPayment->paid_amount) }}
                                        </td>
                                        <td class="text-right">
                                            {{ isset($studentDirectCostPayment) ? number_format(($studentDirectCostPayment->required_amount) -($studentDirectCostPayment->paid_amount)) : 0 }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <hr>

                                @if($studentDirectCostPayment->paid_amount >= $studentDirectCostPayment->required_amount)
                                    <p class="lead">
                                        <span class="alert alert-success">
                                            All amounts paid
                                        </span>
                                    </p>
                                @else

                                    @if($directCostPayments->sum('amount') > 0)
                                        <form method="POST" action="/registrations/{{ $registration->id }}/direct-cost">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="amount"
                                                   value="{{ $directCostPayments->sum('amount') }}">

                                            <button type="submit" class="btn btn-primary">Pay for Direct Cost</button>

                                        </form>
                                    @else
                                        <p class="lead text-center">
                                            You have insufficient balance to pay for this transaction.
                                            <br>
                                            <a href="/help" target="_blank">How to pay</a>
                                        </p>
                                    @endif

                                @endif

                                <hr>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
