@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                @include('partials.sidebar')
            </div>
            <div class="col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }}
                            Semester: {{ $registration->semester }}
                            From: {{ $registration->starting_date->toFormattedDateString() }}
                            Tod: {{ $registration->ending_date->toFormattedDateString() }}
                            Opened: {{ $registration->created_at->diffForHumans() }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Notice</h3>
                                {{ $registration->description }}

                                <br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                @include('registrations.sidebar')
                            </div>
                            <div class="col-xs-9">
                                <h3>My Details</h3>

                                @include('errors.list')

                                <form method="POST" action="/registrations/1/student-details">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}

                                    <input type="hidden" name="student_id" value="{{ $student->id }}">

                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                        <tr>
                                            <th colspan="6">Students Details</th>
                                        </tr>
                                        <tr>
                                            <th>Programme:</th>
                                            <td>
                                                {{ $student->program->name }}
                                            </td>
                                            <th>School:</th>
                                            <td>
                                                {{ $student->school->name }}
                                            </td>
                                            <th>College:</th>
                                            <td>
                                                {{ $student->college->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Department</th>
                                            <td colspan="5">
                                                {{ $student->department->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Last Name/Surname</strong>
                                                <div class="small">
                                                    (As in your certificates)
                                                </div>
                                            </td>
                                            <td>
                                                <strong>Other Names</strong>
                                                <div class="small">
                                                    (Start with First Name)
                                                </div>
                                            </td>
                                            <td>
                                                <strong>
                                                    Sex <br>
                                                    (F/M)
                                                </strong>
                                            </td>
                                            <th>
                                                Nationality
                                            </th>
                                            <td>
                                                <strong>Date of Birth</strong>
                                                <br>
                                                <div class="small">
                                                    (DD/MM/YEAR)
                                                </div>
                                            </td>
                                            <th>
                                                Year of Study
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="last_name" value="{{ $student->last_name }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="first_name" value="{{ $student->first_name }}"
                                                       class="form-control">
                                                <input type="text" name="middle_name"
                                                       value="{{ $student->middle_name }}" class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="gender" value="{{ $student->gender }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="nationality"
                                                       value="{{ $student->nationality }}" class="form-control">
                                            </td>
                                            <td>
                                                <input type="date" name="dob" value="{{ $student->dob }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="yos" value="{{ $student->yos }}"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>O Level School</th>
                                            <th>F4 Index Number</th>
                                            <th>Year</th>
                                            <th>*A Level School/Diploma College</th>
                                            <th>*F6/Diploma Index Number</th>
                                            <th>Year</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="f4_school" value="{{ $student->f4_school }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="f4_index" value="{{ $student->f4_index }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="f4_year" value="{{ $student->f4_year }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="f6_school" value="{{ $student->f6_school }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="f6_index" value="{{ $student->f6_index }}"
                                                       class="form-control">
                                            </td>
                                            <td>
                                                <input type="text" name="f6_year" value="{{ $student->f6_year }}"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <button type="submit" class="btn btn-primary">Update</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
