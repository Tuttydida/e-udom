@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2">
                @include('partials.sidebar')
            </div>
            <div class="col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Academic Year: {{ $registration->academic_year }}
                            Semester: {{ $registration->semester }}
                            From: {{ $registration->starting_date->toFormattedDateString() }}
                            Tod: {{ $registration->ending_date->toFormattedDateString() }}
                            Opened: {{ $registration->created_at->diffForHumans() }}
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Notice</h3>
                                {{ $registration->description }}

                                <br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                @include('registrations.sidebar')
                            </div>
                            <div class="col-xs-9">
                                <h3>Parent Or Guardian</h3>

                                @include('errors.list')

                                <form method="POST" action="/registrations/{{ $registration->id }}/parent-or-guardian">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}

                                    <input type="hidden" name="student_id" value="{{ $student->id }}">

                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                        <tr>
                                            <th colspan="4">Parent/Guardian</th>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td>
                                                <input type="text" name="parent_name"
                                                       value="{{ $student->parent_name }}"
                                                       class="form-control">
                                            </td>
                                            <td>Relationship</td>
                                            <td>
                                                <input type="text" name="parent_relationship"
                                                       value="{{ $student->parent_relationship }}"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td>
                                                <input type="text" name="parent_address"
                                                       value="{{ $student->parent_address }}"
                                                       class="form-control">
                                            </td>
                                            <td>Mobile</td>
                                            <td>
                                                <input type="text" name="parent_mobile"
                                                       value="{{ $student->parent_mobile }}"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <button type="submit" class="btn btn-primary">Update</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
