<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<title>Step Three</title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<div class="jumbotron">
		<div class="container text-center">
			<h1>Step 3/3</h1>
			<h2>Congratulations</h2>	
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">&nbsp;</h3>
					</div>
					<div class="panel-body">
						<h3>Your registration was completed successfully.</h3>
						<ul>
							<li>Print four copies of the registration form</li>
							<li>Submit three copies of the forms to the registry and keep the remaining form in a safe place for future reference.</li>
						</ul>
					</div>
					<div class="panel-footer text-center">
						<a href="/registrations" class="btn btn-primary">Exit</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>