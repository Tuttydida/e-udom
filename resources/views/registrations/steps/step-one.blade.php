<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<title>Step Two</title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<div class="jumbotron">
		<div class="container text-center">
			<h1>Step 1/3</h1>
			<h2>Outstandings</h2>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Your Outstandings</h3>
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-right">SN</th>
									<th>Name</th>
									<th class="text-right">Required</th>
									<th class="text-right">Paid</th>
									<th class="text-right">Remaining</th>
								</tr>
							</thead>
							<tbody>
								@php $i = 1 @endphp
								@foreach($outstandings as $outstanding)
									<tr>
										<td class="text-right">{{ $i++ }}.</td>
										<td>{{ $outstanding['name'] }}</td>
										<td class="text-right">{{ number_format($outstanding['amount']) }}</td>
										<td class="text-right">{{ number_format($outstanding['paid']) }}</td>
										<td class="text-right">{{ number_format($outstanding['remaining']) }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="panel-footer text-center">
						<a href="/registrations" class="btn btn-success btn-lg">&laquo; Back</a>
						<a href="/{{ Request::segment(1) }}/{{ Request::segment(2) }}/steps/step-two" class="btn btn-success btn-lg">Step Two &raquo;</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>