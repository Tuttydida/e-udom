@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h1>Help</h1>
            <div class="col-sm-12">
                <h2>How to pay?</h2>
                <p>
                    Use your mobile money service provider.
                    <br>
                    <strong>Information to have:-</strong>
                <ul>
                    <li>
                        Your Registration number
                    </li>
                    <li>
                        Your year of study
                    </li>
                    <li>
                        Your semester
                    </li>
                    <li>
                        What are you paying for
                    </li>
                </ul>
                </p>
            </div>
        </div>
    </div>

@endsection
