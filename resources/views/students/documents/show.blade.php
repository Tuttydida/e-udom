@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			@include('partials.sidebar')
		</div>
		<div class="col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						{{ $document->documentType->name }}
					</h3>
				</div>
				<div class="panel-body">

          <div class="row">
            <div class="col-sm-4">
              <div class="well">
                1. Head of Department <br><br>
                @if($document->hod_state == 'waiting')
                  <div class="alert alert-info">
                    Waiting...
                  </div>
                @endif
                @if($document->hod_state == 'approved')
                  <div class="alert alert-success">
                   Seen
                  </div>
                @endif
                @if($document->hod_state == 'denined')
                  <div class="alert alert-danger">
                    Denied
                  </div>
                @endif
              </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                2. Dean of School <br><br>
                @if($document->deanS_state == 'waiting')
                  <div class="alert alert-info">
                    Waiting...
                  </div>
                @endif
                @if($document->deanS_state == 'approved')
                  <div class="alert alert-success">
                   Seen
                  </div>
                @endif
                @if($document->deanS_state == 'denined')
                  <div class="alert alert-danger">
                    Denied
                  </div>
                @endif
              </div>
            </div>
            <div class="col-sm-4">
              <div class="well">
                3. Principal of College <br><br>
                @if($document->principal_state == 'waiting')
                  <div class="alert alert-info">
                    Waiting...
                  </div>
                @endif
                @if($document->principal_state == 'approved')
                  <div class="alert alert-success">
                   Seen
                  </div>
                @endif
                @if($document->principal_state == 'denined')
                  <div class="alert alert-danger">
                    Denied
                  </div>
                @endif
              </div>
            </div>
          </div>

				</div>
				<div class="panel-footer">
					Submitted <strong>{{ $document->created_at->diffForHumans() }}</strong>
				</div>
			</div>
      @if($reply)
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Reply</h3>
          </div>
          <div class="panel-body">
            <h3>Congratulation, your request was accepted. You can now download your reply</h3>
            <a href="/documents/{{ $reply->id }}/read" class="btn btn-success btn-lg btn-block" target="_blank">View Reply</a>
          </div>
        </div>
      @endif
		</div>
		<div class="col-sm-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Instructions</h3>
				</div>
				<div class="panel-body">
					You can submit appeals, permissions or postponements letters.
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
