<div class="list-group hidden-xs">
    <a href="/home" class="list-group-item {{ Request::is('home') ? ' active' : '' }}"><i class="fa fa-home fa-fw"></i>
        Home</a>
    @if (auth()->user()->hasRole('student'))
        <a href="/registrations" class="list-group-item {{ Request::is('registrations') ? ' active' : '' }}"><i
                    class="fa fa-pencil fa-fw"></i> Registrations</a>
        <a href="/payments" class="list-group-item {{ Request::is('payments') ? ' active' : '' }}"><i
                    class="fa fa-money fa-fw"></i> Payments</a>
        <a href="/documents" class="list-group-item {{ Request::is('documents') ? ' active' : '' }}"><i
                    class="fa fa-file-pdf-o fa-fw"></i> Documents</a>
    @endif
    @if(auth()->user()->hasRole('bursar'))
        <a href="/finance/payments" class="list-group-item {{ Request::is('payments') ? ' active' : '' }}"><i
                    class="fa fa-money fa-fw"></i> Payments</a>
    @endif
    @if (auth()->user()->hasRole('head-of-department'))
        <a href="/staff/registrations" class="list-group-item"><i class="fa fa-file-pdf-o fa-fw"></i> Registrations</a>
        <a href="/staff/documents" class="list-group-item"><i class="fa fa-file-pdf-o fa-fw"></i> Documents</a>
    @endif
    @if (auth()->user()->hasRole('dean-of-school'))
        <a href="/dean-of-school/registrations" class="list-group-item"><i class="fa fa-file-pdf-o fa-fw"></i> Registrations</a>
        <a href="/dean-of-school/documents" class="list-group-item"><i class="fa fa-file-pdf-o fa-fw"></i> Documents</a>
    @endif
    @if (auth()->user()->hasRole('administrative-officer'))
        <a href="/admin-officer/documents" class="list-group-item"><i class="fa fa-file-pdf-o fa-fw"></i> Documents</a>
    @endif
    @if (auth()->user()->hasRole('principal-of-college'))
        <a href="/principal-of-college/registrations" class="list-group-item"><i class="fa fa-pencil fa-fw"></i>
            Registrations</a>
        <a href="/principal-of-college/documents" class="list-group-item"><i class="fa fa-file-pdf-o fa-fw"></i>
            Documents</a>
    @endif
    <a href="{{ route('notifications') }}" class="list-group-item"><i class="fa fa-info fa-fw"></i> Notifications <span
                class="badge">{{ Auth::user()->unreadNotifications->count() }}</span></a>

@if (auth()->user()->hasRole('admin'))
    @include('partials.admin.sidebar')
@endif
@if(auth()->user()->hasRole('dean-of-students'))
  <div class="dropdown">
    <a href="#" class="list-group-item dropdown-toggle"   data-toggle="dropdown">Accommodation
    <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
      <li role="presentation"><a href="/dean-of-students/accomodation/blocks" role="menuitem" tabindex="-1" href="#">Blocks</a></li>
        <li role="presentation"><a href="/dean-of-students/accomodation/wings/create/constraints" role="menuitem" tabindex="-1" href="#">Create Wing constraints</a></li>
    </ul>
  </div>
@endif
</div>
