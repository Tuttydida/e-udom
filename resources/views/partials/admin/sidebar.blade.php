<div class="list-group hidden-xs">
    <a href="/home" class=" list-group-item {{ Request::is('admin/overview') ? ' active' : '' }}"> <span><i class="fa fa-dashboard"></i> Dashboard</span></a>
    <a href="/admin/university-registrations"
       class="list-group-item  {{ Request::is('admin/university-registrations') ? ' active' : '' }}">
        <span><i class="fa fa-pencil fa-fw"></i> Registrations</span></a>
    <a href="/admin/colleges" class="list-group-item  {{ Request::is('admin/colleges') ? ' active' : '' }}">
        <span><i class="fa fa-book fa-fw"></i> Colleges</span></a>
    <a href="/admin/schools" class="list-group-item  {{ Request::is('admin/schools') ? ' active' : '' }}">
        <span><i class="fa fa-book fa-fw"></i> Schools</span></a>
    <a href="/admin/departments" class="list-group-item  {{ Request::is('admin/departments') ? ' active' : '' }}">
        <span><i class="fa fa-book fa-fw"></i> Departments</span></a>
    <a href="/admin/programs" class="list-group-item  {{ Request::is('admin/programs') ? ' active' : '' }}">
        <span><i class="fa fa-book fa-fw"></i> Programs</span></a>
    <a href="/admin/courses" class="list-group-item  {{ Request::is('admin/courses') ? ' active' : '' }}">
        <span><i class="fa fa-book fa-fw"></i> Courses</span></a>
    <a href="/admin/staff" class="list-group-item {{ Request::is('admin/staff') ? ' active' : '' }} ">
        <span><i class="fa fa-users fa-fw"></i> Staff</span></a>
    <a href="/admin/students" class="list-group-item {{ Request::is('admin/students') ? ' active' : '' }} ">
        <span><i class="fa fa-users fa-fw"></i> Students</span></a>
</div>