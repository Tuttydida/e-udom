@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			@include('partials.sidebar')
		</div>
		<div class="col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						Documents
					</h3>
				</div>
				<div class="panel-body">

					@if($documents->count())
					<table class="table table-striped">
						<thead>
							<tr>
								<th>SN</th>
								<th>Type</th>
								<th>Status</th>
								<th>Date Approved</th>
								<th>Submitted</th>
								<th>Details</th>
							</tr>
						</thead>
						<tbody>
							@php
								$i= 1;
							@endphp
							@foreach($documents as $document)
								<tr>
									<td>{{ $i++ }}.</td>
									<td>{{ $document->documentType->name }}</td>
									<td>
										@if ($document->principal_state == 'approved')
											<span class="text-success">
												Approved
											</span>
										@else
											<span class="badge">Pending</span>
										@endif
									</td>
									<td>
										@if ($document->status == true)
											{{ $document->approved_at->diffForHumans() }}
										@else
											NA
										@endif
									</td>
									<td>{{ $document->created_at->diffForHumans() }}</td>
									<td>
										<a href="/documents/{{ $document->id }}">Read</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@else
						<div class="alert alert-info">
							You have not submitted any document yet.
						</div>
					@endif

				</div>
				<div class="panel-footer">
					<a href="/documents/create" class="btn btn-link">New</a>
				</div>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Instructions</h3>
				</div>
				<div class="panel-body">
					You can submit appeals, permissions or postponements letters.
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
