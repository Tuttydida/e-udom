@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			@include('partials.sidebar')
		</div>
		<div class="col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						Submit your document
					</h3>
				</div>
				<div class="panel-body">

					@include('errors.list')
					
					<form method="POST" action="/documents" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="document_type_id">Request for?</label>
							<select name="document_type_id" id="document_type_id" class="form-control">
								<option value="">-Select-</option>
								@foreach($documentTypes as $documentType)
									<option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
								@endforeach								
							</select>
						</div>
						<div class="form-group">
							<label for="document">Browse for your document</label>
							<input type="file" name="document">
						</div>

						<div class="form-group">
							<p class="help-block">
								<a href="/download/sample" target="_black">View sample</a>
							</p>	
						</div>

						<button type="submit" class="btn btn-primary">Submit</button>

					</form>

				</div>
				<div class="panel-footer">
					<a href="/documents/create" class="btn btn-link">New</a>
				</div>
			</div>
		</div>
		<div class="col-sm-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Instructions</h3>
				</div>
				<div class="panel-body">
					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
