@extends('layouts.app')

@section('page_title', ' | University Registrations')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.admin.sidebar')
            </div>
            <div class="col-sm-10">
                <!-- BASIC TABLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Registered Students
                        </h3>
                    </div>
                    <div class="panel-body">

                        <form action="/reg-report/generate" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $universityRegistration->id }}">

                            <button type="submit" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Export</button>
                        </form>

                        <br>
                        <br>

                        @if($registrations->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Student</th>
                                        <th>Academic Year</th>
                                        <th>Year of Study</th>
                                        <th>Semester</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1 @endphp
                                    @foreach($registrations as $registration)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>{{ $registration->user->username }}</td>
                                        <td>{{ $registration->academic_year }}</td>
                                        <td>{{ $registration->year_of_study }}</td>
                                        <td>{{ $registration->semester }}</td>
                                        <td>{{ $registration->created_at }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">No data</div>
                        @endif

                    </div>
                </div>
                <!-- END BASIC TABLE -->
            </div>
        </div>
    </div>
@endsection
