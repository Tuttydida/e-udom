@extends('layouts.app')

@section('page_title', ' | Create a New College')

@section('content')
       <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
        <div class="col-sm-8">

     <div class="panel panel-heading">
                   <h3 class="page-title">Open a New Registration</h3>
                <div class="panel-body">
                    <form method="POST" action="/admin/university-registrations" class="form-horizontal">
                        {{ csrf_field() }}

                        @include('admin.university-registrations._form')

                       <div class="form-group">
                         <div class="col-sm-5 col-sm-offset-2">
                           <button type="submit" class="btn btn-primary">Open</button>
                         </div>
                       </div>

                    </form>
                </div>
            </div>

        </div>
        <div class="col-sm-2">
        
        </div>
      </div>
    </div>

@endsection
