@extends('layouts.app')

@section('page_title', ' | University Registrations')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.admin.sidebar')
            </div>
            <div class="col-sm-10">
                <!-- BASIC TABLE -->
                <div class="panel">
                    <div class="panel-heading">
                        <p class="panel-subtitle">
                            <a href="/admin/university-registrations/create" class="btn btn-primary">New</a>
                        </p>
                    </div>
                    <div class="panel-body">
                        @if($universityRegistrations->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Academic Year</th>
                                    <th>Semester</th>
                                    <th>starting_date</th>
                                    <th>ending_date</th>
                                    {{-- <th>Status</th> --}}
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th class="text-center"><i class="fa fa-binoculars"></i></th>
                                    <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($universityRegistrations as $universityRegistration)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $universityRegistration->academic_year }}</td>
                                        <td>{{ $universityRegistration->semester }}</td>
                                        <td>{{ $universityRegistration->starting_date }}</td>
                                        <td>{{ $universityRegistration->ending_date }}</td>
                                        {{--             <td>
                                                        @if($universityRegistration->status == false)
                                                            <span class="badge">Open</span>
                                                        @else
                                                            <span class="badge">Closed</span>
                                                        @endif
                                                    </td> --}}
                                        <td>{{ $universityRegistration->created_at }}</td>
                                        <td>{{ $universityRegistration->updated_at }}</td>
                                        <td>
                                            <a href="/admin/university-registrations/{{ $universityRegistration->id }}"><i
                                                        class="fa fa-link"></i></a>
                                        </td>
                                        <td>
                                            <a href="/admin/university-registrations/{{ $universityRegistration->id }}/edit"><i
                                                        class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else

                        @endif
                    </div>
                </div>
                <!-- END BASIC TABLE -->
            </div>
            <div class="col-sm-2">

            </div>
        </div>
    </div>
@endsection
