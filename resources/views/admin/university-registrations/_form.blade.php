<div class="form-group">
    <label for="college_id" class="col-sm-2 control-label">College</label>
    <div class="col-sm-5">
        <select name="college_id" id="college_id" class="form-control">
        	<option value="">-Select-</option>
        	@foreach($colleges as $college)
				<option value="{{ $college->id }}">{{ $college->name }}</option>
        	@endforeach
        </select>
    </div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Academic Year</label>
	<div class="col-sm-5">
	<select name="academic_year" id="academic_year" class="form-control">
		<option value="">-Select-</option>
		<option value="2016/2017">2016/2017</option>
		<option value="2016/2017">2017/2018</option>
	</select>	
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Semester</label>
	<div class="col-sm-5">
	<select name="semester" id="semester" class="form-control">
		<option value="">-Select-</option>
		<option value="I">Semester I</option>
		<option value="II">Semester II</option>
	</select>	
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Starting Date</label>
	<div class="col-sm-5">
	<input type="date" name="starting_date" id="starting_date" class="form-control">	
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label">Ending Date</label>
	<div class="col-sm-5">
	<input type="date" name="ending_date" id="ending_date" class="form-control">	
	</div>
</div>
{{-- <div class="form-group">
	<label class="col-sm-2 control-label">Status</label>
	<div class="col-sm-5">
		<div class="radio">
			<label>
				<input type="radio" name="status" id="status" value="0" checked="checked"> Opened
			</label>
		</div>
		<div class="radio">
			<label>
				<input type="radio" name="status" id="status" value="1"> Closed
			</label>
		</div>
	</div>
</div> --}}

<input type="hidden" name="status" id="status" value="0">

<div class="form-group">
	<label class="col-sm-2 control-label">Notes (Option)</label>
	<div class="col-sm-5">
		<textarea name="description" id="description" class="form-control" rows="3"></textarea>	
	</div>
</div>
