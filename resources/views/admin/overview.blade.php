@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
        <div class="col-sm-8">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Dashboard</h3>
            </div>
            <div class="panel-body">
              
              <div class="row">
                <h1>Registrations of students for each college</h1>
                <div class="col-sm-6">
                  <canvas id="myChart" width="400px" height="400px"></canvas>
                </div>
              </div>

            </div>
          </div>

        </div>
        <div class="col-sm-2">
        
        </div>
      </div>
    </div>

@endsection
