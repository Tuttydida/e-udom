@extends('layouts.app')

@section('page_title', ' | Create a New Permission')

@section('content')
  <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
        <div class="col-md-8">
            <!-- PANEL HEADLINE -->
            <div class="panel panel-headline">
                <div class="panel-body">
                    <form method="POST" action="/admin/permissions" class="form-horizontal">
                        {{ csrf_field() }}

                        @include('admin.permissions._form')

                    </form>
                </div>
            </div>
            <!-- END PANEL HEADLINE -->
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection
