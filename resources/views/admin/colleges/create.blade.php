@extends('layouts.app')

@section('page_title', ' | Create a New College')

@section('content')
       <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
        <div class="col-sm-10">

     <div class="panel panel-headline">
                   <h3 class="page-title">Create a New College</h3>
                <div class="panel-body">
                    <form method="POST" action="/admin/colleges" class="form-horizontal">
                        {{ csrf_field() }}

                        @include('admin.colleges._form')

                    </form>
                </div>
            </div>

        </div>
        <div class="col-sm-2">
        
        </div>
      </div>
    </div>

@endsection
