<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-5">
        <input type="text" name="name" id="name" class="form-control" placeholder="College Name">
    </div>
</div>
<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Principal</label>
    <div class="col-sm-5">
        <select name="principal" id="principal" class="form-control" required="required">
        	<option value="">-Select-</option>
        	@foreach($users as $user)
        		<option value="{{ $user->id }}">{{ $user->username }}</option>
        	@endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>