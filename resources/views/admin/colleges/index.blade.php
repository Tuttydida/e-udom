@extends('layouts.app')

@section('page_title', ' | Colleges')

@section('content')
<div class="container-fluid">
  <div class="row">
 <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
<div class="col-md-10">
<!-- BASIC TABLE -->
<div class="panel">
<div class="panel-heading">
<p class="panel-subtitle">
<a href="/admin/colleges/create" class="btn btn-primary">New</a>
</p>
</div>
<div class="panel-body">
@if($colleges->count() > 0)
<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Created</th>
        <th>Updated</th>
        <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    @foreach($colleges as $college)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $college->name }}</td>
            <td>{{ $college->created_at }}</td>
            <td>{{ $college->updated_at }}</td>
            <td>
                <a href="/admin/colleges/{{ $college->id }}/edit"><i class="fa fa-edit"></i></a>
            </td>
            <td>
    <a class="btn btn-danger" data-toggle="modal" href='#{{ $college->id }}'><i class="fa fa-trash"></i> Delete</a>
    <div class="modal fade" id="{{ $college->id }}">
        <div class="modal-dialog">
        <form action="{{ url('/admin/colleges/'.$college->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete {{ $college->name }}</h4>
                </div>
                <div class="modal-body">

                    Are you sure that you want to delete <strong>{{ $college->name }}</strong>? 
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
                    <button type="submit" id="delete-college-{{ $college->id}}" class="btn btn-danger">
                        <i class="fa fa-btn fa-check">&nbsp;Yes</i>
                        </button>
                </div>
            </div>
            </form>
        </div>
    </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@else

@endif
</div>
</div>
<!-- END BASIC TABLE -->
</div>
<div class="col-sm-2">
   
        </div>
</div>  
</div>
@endsection
