@extends('layouts.app')

@section('page_title', ' | Students')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.admin.sidebar')
            </div>
            <div class="col-sm-10">
                <!-- BASIC TABLE -->
                <div class="panel">
                    <div class="panel-heading">
                        <p class="panel-subtitle">
                            <a href="/admin/students/create" class="btn btn-primary"><i class="fa fa-file-excel-o"></i>
                                Import</a>
                        </p>
                    </div>
                    <div class="panel-body">
                        @if($students->count() > 0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Reg No</th>
                                    <th>Program</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{ $i++ }}</td>

                                        <td><a href="#">{{ $student->user->username }}</a></td>

                                        <td>{{ $student->program->name }}</td>
                                        <td>{{ $student->created_at->toFormattedDateString() }}</td>
                                        <td>{{ $student->updated_at->toFormattedDateString() }}</td>
                                        <td>
                                            <a href="/admin/students/{{ $student->id }}/edit"><i class="fa fa-edit"></i></a>
                                        </td>
                                        <td>
                                            <a class="text-danger" data-toggle="modal" href='#{{ $student->id }}'><i
                                                        class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="{{ $student->id }}">
                                                <div class="modal-dialog">
                                                    <form action="{{ url('/admin/students/'.$student->id) }}"
                                                          method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                                <h4 class="modal-title">Delete {{ $student->name }}</h4>
                                                            </div>
                                                            <div class="modal-body">

                                                                Are you sure that you want to delete
                                                                <strong>{{ $student->name }}</strong>?

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal"><i
                                                                            class="fa fa-remove"></i> No
                                                                </button>
                                                                <button type="submit"
                                                                        id="delete-college-{{ $student->id}}"
                                                                        class="btn btn-danger">
                                                                    <i class="fa fa-btn fa-check">&nbsp;Yes</i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else

                        @endif
                    </div>
                </div>
                <!-- END BASIC TABLE -->
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection
