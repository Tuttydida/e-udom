<div class="form-group">
    <label for="program_id" class="col-sm-2 control-label">Program</label>
    <div class="col-sm-5">
        <select name="program_id" id="program_id" class="form-control">
            <option value="">-Select-</option>
            @foreach($programs as $program)
                <option value="{{ $program->id }}">{{ $program->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="user_id" class="col-sm-2 control-label">Students</label>
    <div class="col-sm-5">
        <input type="file" name="user_file">
        <p class="help-block">
            Click <a href="#">here</a> to download sample
        </p>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
    </div>
</div>