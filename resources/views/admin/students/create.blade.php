@extends('layouts.app')

@section('page_title', ' | Create a New Student')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.admin.sidebar')
            </div>
            <div class="col-md-10">
                <!-- PANEL HEADLINE -->
                <div class="panel panel-headline">
                    <h3 class="page-title">Upload Students</h3>
                    <div class="panel-body">
                        <form method="POST" action="/admin/students" enctype="multipart/form-data"
                              class="form-horizontal" files = true>
                            {{ csrf_field() }}

                            @include('admin.students._form')

                        </form>
                    </div>
                </div>
                <!-- END PANEL HEADLINE -->
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection
