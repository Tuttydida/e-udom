<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-5">
        <input type="text" name="name" id="name" class="form-control" placeholder="Department Name">
    </div>
</div>
<div class="form-group">
    <label for="school_id" class="col-sm-2 control-label">School</label>
    <div class="col-sm-5">
        <select name="school_id" id="school_id" class="form-control">
        	<option value="">-Select-</option>
        	@foreach($schools as $school)
				<option value="{{ $school->id }}">{{ $school->name }}</option>
        	@endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="head" class="col-sm-2 control-label">Head of Department</label>
    <div class="col-sm-5">
        <select name="head" id="head" class="form-control">
            <option value="">-Select-</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->username }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>