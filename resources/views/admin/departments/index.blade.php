@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
  <div class="col-md-10">
    <!-- BASIC TABLE -->
    <div class="panel">
        <div class="panel-heading">
            <p class="panel-subtitle">
                <a href="/admin/departments/create" class="btn btn-primary">New</a>
            </p>
        </div>
        <div class="panel-body">
            @if($departments->count() > 0)
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Head</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($departments as $department)
                        <tr>
                            <td class="text-right">{{ $i++ }}.</td>
                            <td>{{ $department->name }}</td>
                            <td>{{ $department->hod->username }}</td>
                            <td>{{ $department->created_at }}</td>
                            <td>{{ $department->updated_at }}</td>
                            <td>
                                <a href="/admin/departments/{{ $department->id }}/edit"><i class="fa fa-edit"></i></a>
                            </td>
                      <td>
    <a class="btn btn-danger" data-toggle="modal" href='#{{ $department->id }}'><i class="fa fa-trash"></i> Delete</a>
    <div class="modal fade" id="{{ $department->id }}">
        <div class="modal-dialog">
        <form action="{{ url('/admin/departments/'.$department->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete {{ $department->name }}</h4>
                </div>
                <div class="modal-body">

                    Are you sure that you want to delete <strong>{{ $department->name }}</strong>? 
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
                    <button type="submit" id="delete-department-{{ $department->id}}" class="btn btn-danger">
                        <i class="fa fa-btn fa-check">&nbsp;Yes</i>
                        </button>
                </div>
            </div>
            </form>
        </div>
    </div>
            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else

            @endif
        </div>
    </div>
    <!-- END BASIC TABLE -->
</div>
        <div class="col-sm-2">
        
        </div>
      </div>
    </div>

@endsection

