<div class="form-group">
    <label for="display_name" class="col-sm-2 control-label">Display Name</label>
    <div class="col-sm-5">
        <input type="text" name="display_name" id="display_name" class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-5">
        <textarea name="description" id="description" rows="5" maxlength="255" class="form-control"></textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>