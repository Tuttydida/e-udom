<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-5">
        <input type="text" name="name" id="name" class="form-control" placeholder="Program Name">
    </div>
</div>
<div class="form-group">
    <label for="duration" class="col-sm-2 control-label">Duration (Years)</label>
    <div class="col-sm-5">
        <input type="number" name="duration" id="duration" class="form-control" placeholder="Program Duration">
    </div>
</div>
<div class="form-group">
    <label for="tuition_fee_type_id" class="col-sm-2 control-label">Tuition Fee (Yearly)</label>
    <div class="col-sm-5">
        <select name="tuition_fee_type_id" id="tuition_fee_type_id" class="form-control">
            <option value="">-Select-</option>
            @foreach($tuition_fee_types as $tuition_fee_type)
                <option value="{{ $tuition_fee_type->id }}">{{ $tuition_fee_type->amount }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="department_id" class="col-sm-2 control-label">Department</label>
    <div class="col-sm-5">
        <select name="department_id" id="department_id" class="form-control">
        	<option value="">-Select-</option>
        	@foreach($departments as $department)
				<option value="{{ $department->id }}">{{ $department->name }}</option>
        	@endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>