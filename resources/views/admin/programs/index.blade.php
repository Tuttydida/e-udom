@extends('layouts.app')

@section('content')
@section('page_title', ' | Programs')
  <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
<div class="col-md-10">
<!-- BASIC TABLE -->
<div class="panel">
<div class="panel-heading">
<p class="panel-subtitle">
    <a href="/admin/programs/create" class="btn btn-primary">New</a>
</p>
</div>
<div class="panel-body">
@if($programs->count() > 0)
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Duration (Years)</th>
            <th>Created</th>
            <th>Updated</th>
            <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
        @foreach($programs as $program)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $program->name }}</td>
                <td>{{ $program->duration }}</td>
                <td>{{ $program->created_at }}</td>
                <td>{{ $program->updated_at }}</td>
                <td>
                    <a href="/admin/program/{{ $program->id }}/edit"><i class="fa fa-edit"></i></a>
                </td>
                   <td>
    <a class="btn btn-danger" data-toggle="modal" href='#{{ $program->id }}'><i class="fa fa-trash"></i> Delete</a>
    <div class="modal fade" id="{{ $program->id }}">
        <div class="modal-dialog">
        <form action="{{ url('/admin/programs/'.$program->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete {{ $program->name }}</h4>
                </div>
                <div class="modal-body">

                    Are you sure that you want to delete <strong>{{ $program->name }}</strong>? 
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> No</button>
                    <button type="submit" id="delete-college-{{ $program->id}}" class="btn btn-danger">
                        <i class="fa fa-btn fa-check">&nbsp;Yes</i>
                        </button>
                </div>
            </div>
            </form>
        </div>
    </div>
            </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else

@endif
</div>
</div>
<!-- END BASIC TABLE -->
</div>
</div>
<div class="col-md-2">
    
</div>
@endsection
