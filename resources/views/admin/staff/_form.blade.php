<div class="form-group">
	<label for="username">Username</label>
	<input type="text" name="username" id="username" value="{{ isset($user->username) ? $user->username : old('username') }}" class="form-control">
</div>
<div class="form-group">
	<label for="password">Password</label>
	<input type="password" name="password" id="password" class="form-control">
</div>
<button type="submit" class="btn btn-primary">{{ $btn_text }}</button>