@extends('layouts.app')

@section('page_title', ' | Students')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.admin.sidebar')
            </div>
            <div class="col-md-10">
                <!-- BASIC TABLE -->
                <div class="panel">
                    <div class="panel-heading">
                        <p class="panel-subtitle">
                            <a href="/admin/students/create" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Import</a>
                        </p>
                    </div>
                     <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">
                    @if($users)
                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Added</th>
                                    <th>Last updated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; $is_student=false;?>
                                @foreach($users as $user)
                                  @foreach($user->roles as $role)
                                                @if($role->name=='student')
                                                   <?php $is_student=true;?>
                                                @endif
                                                @endforeach
                                    @if($is_student==false)
                                           <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $user->username }}</td>
                                          <td>{{ $user->email }}</td>
                                            <td>
                                            @if($user->roles()->count())
                                                @foreach($user->roles as $role)
                                                    <span class="badge">{{ $role->name }}</span>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>{{ $user->created_at->diffForHumans() }}</td>
                                        <td>{{ $user->updated_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="/admin/staff/{{ $user->id }}/edit">Edit</a>
                                            <a href="#">Delete</a>
                                        </td>
                                    </tr>
                                    @endif
                                <?php $is_student =false; ?>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        No users
                    @endif
                </div>
            </div>
                </div>
                <!-- END BASIC TABLE -->
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
@endsection
