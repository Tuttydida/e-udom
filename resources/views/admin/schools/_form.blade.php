<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-5">
        <input type="text" name="name" id="name" class="form-control" placeholder="School Name">
    </div>
</div>
<div class="form-group">
    <label for="college_id" class="col-sm-2 control-label">College</label>
    <div class="col-sm-5">
        <select name="college_id" id="college_id" class="form-control">
        	<option value="">-Select-</option>
        	@foreach($colleges as $college)
				<option value="{{ $college->id }}">{{ $college->name }}</option>
        	@endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Dean</label>
    <div class="col-sm-5">
        <select name="dean" id="dean" class="form-control" required="required">
            <option value="">-Select-</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->username }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>