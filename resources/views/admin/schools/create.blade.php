@extends('layouts.app')

@section('page_title', 'Dashboard | ')

@section('content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
        <div class="col-sm-10">
            <!-- PANEL HEADLINE -->
            <div class="panel panel-headline">
               <h3 class="page-title">Create a New School</h3>
                <div class="panel-body">
                    <form method="POST" action="/admin/schools" class="form-horizontal">
                        {{ csrf_field() }}

                        @include('admin.schools._form')

                    </form>
                </div>
            </div>
            <!-- END PANEL HEADLINE -->
        </div>
    </div>
          </div>
        <div class="col-sm-2">
        
        </div>
      </div>
    </div>

@endsection
