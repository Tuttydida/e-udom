<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-5">
        <input type="text" name="name" id="name" class="form-control" placeholder="Course Name">
    </div>
</div>
<div class="form-group">
    <label for="credits" class="col-sm-2 control-label">Credits</label>
    <div class="col-sm-5">
        <input type="number" name="credits" id="credits" class="form-control" placeholder="Course credits">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>