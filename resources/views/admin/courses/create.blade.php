@extends('layouts.app')

@section('page_title', ' | Create a New Course')

@section('content')
     <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2">
          @include('partials.admin.sidebar')
        </div>
        <div class="col-md-10">
            <!-- PANEL HEADLINE -->
            <div class="panel panel-headline">
                   <h3 class="page-title">Create a New Course</h3>
                <div class="panel-body">
                    <form method="POST" action="/admin/courses" class="form-horizontal">
                        {{ csrf_field() }}

                        @include('admin.courses._form')

                    </form>
                </div>
            </div>
            <!-- END PANEL HEADLINE -->
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection
