@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Payments</div>

                <div class="panel-body">
                   @if ($payments->count())
                    <table class="table table-table-hover">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Amount</th>
                                <th>Receipt #</th>
                                <th>Date of Payment</th>
                                <th>Year of study</th>
                                <th>Semester</th>
                                <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i = 1; @endphp
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $i++ }}.</td>
                                    <td>{{ $payment->amount }}</td>
                                    <td>{{ $payment->receipt_number }}</td>
                                    <td>{{ $payment->date_of_payment->toFormattedDateString() }}</td>
                                    <td>{{ $payment->year_of_study }}</td>
                                    <td>{{ $payment->semester }}</td>
                                    <td>{{ $payment->category }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                   @else
                        <div class="alert alert-info">
                            Currently there are no any payments made.
                        </div>
                   @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
