@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $document->documentType->name }}</div>

                    <div class="panel-body">
                        <a href="/documents/{{ $document->id }}/read" target="_blank">
                            <i class="fa fa-file-pdf-o fa-2x"></i>
                        </a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Principal comments</h3>
                    </div>
                    <div class="panel-body">
                        {{ $document->principal_comment }}
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Replies</h3>
                    </div>
                    <div class="panel-body">
                        
                        @if($document->replies()->count())
                        <ul>
                            @foreach($document->replies as $reply)
                                <li>{{ $reply->created_at }}</li>
                            @endforeach                            
                        </ul>
                        @else
                            <p class="lead">0 replies</p>
                        @endif

                    </div>
                    <div class="panel panel-footer">
                        <form method="POST" enctype="multipart/form-data" action="/admin-officer/documents/{{ $document->id }}/reply">
                            {{ csrf_field() }}

                            <input type="hidden" name="document_id" value="{{ $document->id }}">

                            <div class="form-group">
                                <label for="reply">Upload Reply</label>
                                <input type="file" name="reply">
                            </div>

                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-upload"></i></button>

                        </form>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
