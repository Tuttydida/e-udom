@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Documents</div>

                <div class="panel-body">

                    @if($documents->count())
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Student</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Read</th>
                                <th>Submitted</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i= 1;
                            @endphp
                            @foreach($documents as $document)
                                <tr>
                                    <td>{{ $i++ }}.</td>
                                    <td>{{ $document->user->username }}</td>
                                    <td>{{ $document->documentType->name }}</td>
                                    <td>
                                      @if($document->hod_state == 'waiting')
                                        Waiting...
                                      @endif
                                      @if($document->hod_state == 'approved')
                                        Approved
                                      @endif
                                      @if($document->hod_state == 'denined')
                                        Denied
                                      @endif
                                    </td>
                                    <td>
                                        <a href="/staff/documents/{{ $document->id }}">
                                            Read
                                        </a>
                                    </td>
                                    <td>{{ $document->created_at->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="alert alert-info">
                           No documents!
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
