@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $document->documentType->name }}</div>

                    <div class="panel-body">
                        <a href="/documents/{{ $document->id }}/read" target="_blank">
                            <i class="fa fa-file-pdf-o fa-2x"></i>
                        </a>
                    </div>
                    <div class="panel-footer">

                        <h3>Actions</h3>

                        <form method="POST" action="/staff/documents/{{ $document->id }}/approve">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="radio">
                                <label>
                                    <input type="radio" name="hod_state" value="approved"> Approve
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="hod_state" value="denined" checked="checked"> Disapprove
                                </label>
                            </div>

                            <div class="form-group">
                                <label for="hod_comment">Comment (Option)</label>
                                <textarea name="hod_comment" id="hod_comment" rows="5" class="form-control"
                                          placeholder="Write something..."></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
