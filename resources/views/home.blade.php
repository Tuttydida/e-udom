@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-sm-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        @if(auth()->user()->hasRole('admin'))

                            <div class="row">
                                <div class="col-sm-3 text-center">
                                    <div class="well">
                                        <h1>{{ $stats['colleges_count'] }}</h1>
                                        <h4>COLLEGES</h4>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center">
                                    <div class="well">
                                        <h1>{{ $stats['schools_count'] }}</h1>
                                        <h4>SCHOOLS</h4>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center">
                                    <div class="well">
                                        <h1>{{ $stats['departments_count'] }}</h1>
                                        <h4>DEPARTMENTS</h4>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center">
                                    <div class="well">
                                        <h1>{{ $stats['students_count'] }}</h1>
                                        <h4>STUDENTS</h4>
                                    </div>
                                </div>
                            </div>

                        @endif

                        @if (auth()->user()->hasRole('student'))
                            <h3>Welcome Student</h3>
                            <h4>You have access to the following</h4>
                            <ul>
                                <li>To view your registrations</li>
                                <li>To view your payments</li>
                                <li>To submit documents</li>
                            </ul>
                        @endif
                        @if (auth()->user()->hasRole('head-of-department'))
                            <h3>Welcome Head of Department</h3>
                            <h4>You have access to the following</h4>
                            <ul>
                                <li>To view submitted documents</li>
                                <li>To approve submitted documents</li>
                            </ul>
                        @endif
                        @if (auth()->user()->hasRole('dean-of-school'))
                            <h3>Welcome Dean of School</h3>
                            <h4>You have access to the following</h4>
                            <ul>
                                <li>To view submitted documents</li>
                                <li>To approve submitted documents</li>
                            </ul>
                        @endif
                        @if (auth()->user()->hasRole('principal-of-college'))
                            <h3>Welcome Principal of College</h3>
                            <h4>You have access to the following</h4>
                            <ul>
                                <li>To view submitted documents</li>
                                <li>To approve submitted documents</li>
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
