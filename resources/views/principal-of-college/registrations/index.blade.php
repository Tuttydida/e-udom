@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Registrations</div>

                <div class="panel-body">

                    @if($registrations->count())
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Reg #</th>
                                    <th>Academic Year</th>
                                    <th>Year of Study</th>
                                    <th>Semester</th>
                                    <th>Date of Registration</th>
                                    <th>Program</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach($registrations as $registration)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $registration->user->username }}</td>
                                    <td>{{ $registration->academic_year }}</td>
                                    <td>{{ $registration->year_of_study }}</td>
                                    <td>{{ $registration->semester }}</td>
                                    <td>{{ $registration->date_of_registration }}</td>
                                    <td>{{ $registration->user->student->program->name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info">
                            No registrations
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
