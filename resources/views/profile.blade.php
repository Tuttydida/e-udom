@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile</div>

                    <div class="panel-body">
                        @if(Auth::user()->hasRole('student'))
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th>Reg#</th>
                                    <td>{{ Auth::user()->username }}</td>
                                </tr>
                                <tr>
                                    <th>First Name</th>
                                    <td>
                                        <input type="text" name="first_name" value="{{ $student->first_name }}"
                                               class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Middle Name</th>
                                    <td>
                                        <input type="text" name="middle_name" value="{{ $student->middle_name }}"
                                               class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Last Name</th>
                                    <td>
                                        <input type="text" name="last_name" value="{{ $student->last_name }}"
                                               class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td>
                                        {{ $student->gender }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Date of Birth</th>
                                    <td>
                                        {{ $student->dob }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>
                                        <input type="email" name="email" value="{{ Auth::user()->email }}"
                                               class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nationality</th>
                                    <td>
                                        <input type="text" name="nationality" value="{{ $student->nationality }}"
                                               class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <th>College</th>
                                    <td>
                                        {{ $student->college->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>School</th>
                                    <td>
                                        {{ $student->school->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Department</th>
                                    <td>
                                        {{ $student->department->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Program</th>
                                    <td>
                                        {{ $student->program->name }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
