@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">University Registrations</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>College</th>
                                <th>Academic Year</th>
                                <th>Starting Date</th>
                                <th>Ending Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($university_registrations as $university_registration)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>Some College</td>
                                    <td>{{ $university_registration->academic_year }}</td>
                                    <td>{{ $university_registration->starting_date }}</td>
                                    <td>{{ $university_registration->ending_date }}</td>
                                    <td>{{ $university_registration->status }}</td>
                                    <td>
                                        <a href="#"><i class="fa fa-edit"></i> Edit</a>
                                        |
                                        <a href="#" class="text-danger"><i class="fa fa-trash"> Delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('partials.sidebar')
        </div>
    </div>
</div>
@endsection
