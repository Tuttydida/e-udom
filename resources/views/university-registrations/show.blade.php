@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Registration Information</div>

                <div class="panel-body">

                    {{ $university_registration->description }}

                    
                    <h3>NOTICE</h3>

                    All students will have to register online (<a href="#">link</a>) and offsite from 
                    <strong>
                        {{ $university_registration->starting_date->toFormattedDateString() }}
                    </strong> 
                    until 
                    <strong>
                        {{ $university_registration->ending_date->toFormattedDateString() }}
                    </strong>
                    .

                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('partials.sidebar')
        </div>
    </div>
</div>
@endsection
