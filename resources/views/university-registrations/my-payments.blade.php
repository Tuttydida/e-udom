@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Your dues</div>

                <div class="panel-body">

                   
                    <h1>All dues here!</h1>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('partials.sidebar')
        </div>
    </div>
</div>
@endsection
