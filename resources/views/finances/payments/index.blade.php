@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-sm-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Payments</div>

                    <div class="panel-body">

                        @if($payments->count())
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-right">SN</th>
                                    <th>Reg #</th>
                                    <th class="text-right">Amount</th>
                                    <th>Receipt</th>
                                    <th>Date</th>
                                    <th>Student</th>
                                    <th>College</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($payments as $payment)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>
                                            {{ $payment->student->user->username }}
                                        </td>
                                        <td class="text-right">{{ number_format($payment->amount) }}</td>
                                        <td>{{ $payment->receipt_number }}</td>
                                        <td>{{ $payment->date_of_payment->toFormattedDateString() }}</td>
                                        <td>
                                            {{ $payment->student->first_name }}
                                            {{ $payment->student->middle_name }}
                                            {{ $payment->student->last_name }}
                                        </td>
                                        <td>
                                            {{ $payment->student->college->name }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                </tbody>
                            </table>
                        @else
                            <p class="lead">
                                No Payments
                            </p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
