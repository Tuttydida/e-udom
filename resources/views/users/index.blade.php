@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
       <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>

                <div class="panel-body">
                    @if($users)
                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Username</th>
                                    <th>Added</th>
                                    <th>Last updated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->created_at->diffForHumans() }}</td>
                                        <td>{{ $user->updated_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="/users/{{ $user->id }}/edit">Edit</a>
                                            <a href="#">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        No users
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
