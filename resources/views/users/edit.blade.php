@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Edit {{ $user->username }}</div>

                <div class="panel-body">
                    <form method="POST" action="/users/{{ $user->id }}">
                        {{ csrf_field() }}

                        {{ method_field('PATCH')}}

                        @include('users._form', ['btn_text' => 'Update'])

                    </form>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Roles</h3>
                </div>
                <div class="panel-body">
                    @if($user_roles)
                        <ul class="list-unstyled">
                            @foreach($user_roles as $user_role)
                                <li>{{ $user_role->name }}</li>
                            @endforeach
                        </ul>
                    @else
                        No role
                    @endif
                </div>
                <div class="panel-footer">

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                      Add
                    </button>

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add Role</h4>
                          </div>
                          <div class="modal-body">
                            <form method="POST" action="/users/{{ $user->id }}/addRoles">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="role_id">Select Role</label>
                                    <select name="role_id" id="role_id" class="form-control">
                                        <option value="">-Select-</option>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach    
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">Add</button>

                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                </div>
            </div>
        </div>
        <div class="col-md-3">
            @include('partials.sidebar')
        </div>
    </div>
</div>
@endsection
