<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>404</title>
</head>
<body>
	<div class="jumbotron">
		<div class="container">
			<h1>404</h1>
			<p>We could not find the page you are looking for.</p>
			<p>
				<a href="/" class="btn btn-primary btn-lg">Go home</a>
			</p>
		</div>
	</div>
</body>
</html>