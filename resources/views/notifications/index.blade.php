@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Unread Notifications</h3>
                    </div>
                    <div class="panel-body">
                        @if($notifications->count())
                            <ul>
                                @foreach($notifications as $notification)
                                    <li>
                                        @include('notifications.'.snake_case(class_basename($notification->type)))
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            <div class="alert alert-info">
                                No unread notifications
                            </div>
                        @endif
                    </div>
                    @if($notifications->count())
                        <div class="panel-footer">
                            <form method="POST" action="{{ route('notifications') }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" class="btn btn-primary">Mark All As Read</button>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection